package com.kidsperform;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kidsperform.MainActivity.MenuAdapter;
import com.kidsperform.VoteItemActivity.IgnoreCaseComparator;
import com.kidsperform.data.MenuList;
import com.kidsperform.data.VoteEList;
import com.kidsperform.lib.APIService;
import com.kidsperform.lib.CheckInternetAccess;
import com.kidsperform.lib.JsonHelper;
import com.kidsperform.utils.MyTextView;
import com.kidsperform.utils.ReturnMessage;
import com.kidsperform.R;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class VoteProcessActivity extends Fragment {
	protected MainActivity parentActivity;
	List<VoteEList> votenowlist = new ArrayList<VoteEList>();
	private ListView lvvote;
	View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.vote_main, null);
        lvvote = (ListView)view.findViewById(R.id.listView1);
        parentActivity = (MainActivity)getActivity();
        
        
       
        return view;
    }
    @Override
	public void onStart() {
        super.onStart();

        System.gc();
        new GetEventTask().execute();
    }
	private void onVoteItemClick1(AdapterView<?> parent, View view,
			int position, long id) {
		parentActivity.selectednowlist = votenowlist.get(position);
		parentActivity.setFragment(1, votenowlist.get(position).title);
	}
	public class VoteAdapter extends ArrayAdapter<VoteEList>
	{


		private List<VoteEList>items;
		public VoteAdapter(Context context, int textViewResourceId,
				List<VoteEList> objects) {
			
			super(context, textViewResourceId, objects);
			this.items=objects;
			// TODO Auto-generated constructor stub
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			LayoutInflater vi = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final VoteEList item=items.get(position);
			if(v==null)
			{
				v = vi.inflate(R.layout.votenowlist, null);
			}
			final int po = position;
			MyTextView title = (MyTextView)v.findViewById(R.id.title);
			String s = item.title;
			int f = (int)((float)35 * parentActivity.depth / 2.0f);
			if(item.title.length() > f){
				s = item.title.substring(0, f) + "...";
			}
			title.setText(s);
			MyTextView topvname = (MyTextView)v.findViewById(R.id.topname);
			String s1 = item.topvname;
			f = (int)((float)25 * parentActivity.depth / 2.0f);
			if(s1.length() > f){
				s1 = s1.substring(0,f) + "...";
			}
			topvname.setText(s1);
			return v;
		}
		
	}
	private class GetEventTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetEventTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(!_result.equals("")){
					try {
						JSONObject data = new JSONObject(_result);
						String error = data.get("status").toString();

				        votenowlist.clear();
						if(error.equals("1")){
							String s = data.get("content").toString();
							
							JSONObject content = new JSONObject(s);
							
							JsonHelper helper = new JsonHelper();
							LinkedHashMap<String, Object> map = helper.toMap(content);
							Set<String> keys = map.keySet();

					        List<String> keyColList = new ArrayList<String>();
					        keyColList.addAll(keys);
					        IgnoreCaseComparator icc = new IgnoreCaseComparator();
					        java.util.Collections.sort(keyColList,icc);
					        
							List<VoteEList> votenowlist1 = new ArrayList<VoteEList>();
							for (String keyIndex:keyColList) {
								VoteEList vlist = new VoteEList();
								JSONObject json = (JSONObject)helper.toJSON(map.get(keyIndex));
								String backgroundPathUrl = json.getString("backgroundPathUrl");
								String voteEventid = json.getString("voteEventid");
								String createddate = json.getString("createddate");
								String title = json.getString("title");
								String topVoteName = json.getString("topVoteName");
								String checkfree = json.getString("checkfree");
								String freecount = json.getString("freecount");
								vlist.burl = backgroundPathUrl;
								int ck = 0;
								if(checkfree != null && !checkfree.equals("")){
									ck = Integer.parseInt(checkfree);
								}
								vlist.checkfree = ck;
								vlist.date = createddate;
								vlist.title = title;
								vlist.topvname = topVoteName;
								int vid = 0;
								if(voteEventid != null && !voteEventid.equals("")){
									vid = Integer.parseInt(voteEventid);
								}
								vlist.v_event_id = vid;
								int fid = 0;
								if(freecount != null && !freecount.equals("")){
									fid = Integer.parseInt(freecount);
								}
								vlist.freecount = fid;
								votenowlist.add(vlist);	
							}
							//for(int i = votenowlist1.size()-1 ; i >= 0; i--){
								//VoteEList vlist = votenowlist1.get(i);
								//votenowlist.add(vlist);
							//}
							VoteProcessActivity.VoteAdapter menuadapter1 = new VoteProcessActivity.VoteAdapter(view.getContext(), R.layout.votenowlist, votenowlist);
					        lvvote.setAdapter(menuadapter1);
					        lvvote.setSmoothScrollbarEnabled(true);
					        lvvote.setOnItemClickListener(new OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
									onVoteItemClick1(parent, view, position, id);
								}
			
							});
						}else{
							String content = data.get("content").toString();
							ReturnMessage.showAlartDialog(parentActivity, content);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						ReturnMessage.showAlartDialog(parentActivity, "Service failed.");
					}
				}else{
					ReturnMessage.showAlartDialog(parentActivity, "Can not your device token ID. Please try one more.");
				}
			}else{
				ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(parentActivity, "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
				APIService service = new APIService(1,0);
				String parameter = "reg_id="+parentActivity.tokenid;
				result = service.getContentData(parameter);
	        }
			return result;
		}
	}
	class IgnoreCaseComparator implements Comparator<String> {
		  public int compare(String strA, String strB) {
			  int a = Integer.parseInt(strA);
			  int b = Integer.parseInt(strB);
			  int k = 0;
			  
		    return a-b;//strA.compareToIgnoreCase(strB);
		  }
		}
}
