package com.kidsperform;


import java.util.ArrayList;
import java.util.List;

import com.kidsperform.MainActivity.MenuAdapter;
import com.kidsperform.data.MenuList;
import com.kidsperform.data.VoteEList;
import com.kidsperform.data.VoteItemList;
import com.kidsperform.lib.ImageLoader;
import com.kidsperform.lib.ImageLoader2;
import com.kidsperform.utils.MyTextView;
import com.kidsperform.R;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class VoteConfirmActivity extends Fragment implements OnClickListener{
	protected MainActivity parentActivity;
	VoteItemList voteitemlist = new VoteItemList();
	private ListView lvvote;
	View view;	
	LinearLayout votebt;
	String terms = "This is terms & conditions of payment";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.vote_confirm, null);
        parentActivity = (MainActivity)getActivity();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        votebt = (LinearLayout)view.findViewById(R.id.vote_layout);
        votebt.setOnClickListener(this);
        
        
        return view;
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.vote_layout){
			parentActivity.setFragment1(1, "Vote");
		}
	}
}
