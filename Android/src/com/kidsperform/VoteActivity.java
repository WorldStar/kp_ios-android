package com.kidsperform;

import static com.kidsperform.utils.TouchImageView.State.DRAG;
import static com.kidsperform.utils.TouchImageView.State.FLING;
import static com.kidsperform.utils.TouchImageView.State.NONE;
import pl.polidea.view.ZoomView;

import com.google.android.gcm.GCMRegistrar;
import com.kidsperform.utils.CustomViewPager;
import com.kidsperform.utils.MyButton;
import com.kidsperform.utils.TouchImageView;
import com.kidsperform.utils.ViewPagerAdapter;
import com.kidsperform.R;
import com.kidsperform.R.anim;
import com.kidsperform.R.drawable;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ImageView.ScaleType;

public class VoteActivity extends Fragment implements OnClickListener{
	LinearLayout slide_layout_1,slide_layout_2,slide_layout_3, votebt_layout;
	MyButton slide_left, slide_right;
	public int num = 0;
	protected MainActivity parentActivity;
	TouchImageView image1, image2, image3;
    
	View view;
	private int imageArra[] = { R.drawable.vote_slide_1, R.drawable.vote_slide_2,
			R.drawable.vote_slide_3 };
	ViewPager myPager;
	public static int slidepos = 0;
	int screenWidth, screenHeight;
	BitmapFactory.Options option;
    private PointF last = new PointF();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.voteslide, null);
        parentActivity = (MainActivity)getActivity();
        slide_layout_1 = (LinearLayout)view.findViewById(R.id.slide_layout_1);
        slide_layout_2 = (LinearLayout)view.findViewById(R.id.slide_layout_2);
        slide_layout_3 = (LinearLayout)view.findViewById(R.id.slide_layout_3);
        slide_layout_2.setVisibility(View.GONE);
        slide_layout_3.setVisibility(View.GONE);
        slide_layout_1.setOnTouchListener(new TouchLayoutListener());
        slide_layout_2.setOnTouchListener(new TouchLayoutListener());
        slide_layout_3.setOnTouchListener(new TouchLayoutListener());
        votebt_layout = (LinearLayout)view.findViewById(R.id.votebt_layout);
        votebt_layout.setVisibility(View.VISIBLE);
        votebt_layout.setOnClickListener(this);
        slide_left = (MyButton)view.findViewById(R.id.slide_left);
        slide_left.setVisibility(View.GONE);
        slide_right = (MyButton)view.findViewById(R.id.slide_right);
        slide_left.setOnClickListener(this);
        slide_right.setOnClickListener(this);
        
		option = new BitmapFactory.Options();
		option.inSampleSize = 1;
		option.inPurgeable = true;
		option.inDither = true;

		Display display = parentActivity.getWindowManager().getDefaultDisplay();
		screenWidth = display.getWidth();
		screenHeight = display.getHeight();
		image1 = (TouchImageView)view.findViewById(R.id.image1);
        image2 = (TouchImageView)view.findViewById(R.id.image2);
        image3 = (TouchImageView)view.findViewById(R.id.image3);
        image1.setImageBitmap(BitmapFactory.decodeResource(getResources(),
        		R.drawable.vote_slide_1, option));
        image2.setImageBitmap(BitmapFactory.decodeResource(getResources(),
        		R.drawable.vote_slide_2, option));
        image3.setImageBitmap(BitmapFactory.decodeResource(getResources(),
        		R.drawable.vote_slide_3, option));
        image1.setScaleY(1.1f);
        image2.setScaleY(1.1f);
        image3.setScaleY(1.1f);
        
		
        /*
        */
        /*
        image1 = (TouchImageView)view.findViewById(R.id.image1);
        image2 = (TouchImageView)view.findViewById(R.id.image2);
        image3 = (TouchImageView)view.findViewById(R.id.image3);
        image1.setScaleType(ScaleType.FIT_XY);
        image2.setScaleType(ScaleType.FIT_XY);
        image3.setScaleType(ScaleType.FIT_XY);
        //image1.setMinZoom(image1.getCurrentZoom());
        //image1.setMinimumHeight(parentActivity.height);
        image1.setMinZoom(0);
        image1.setMaxZoom(5);
        //image2.setMinZoom(image1.getCurrentZoom());
        image2.setMaxZoom(5);
        //image3.setMinZoom(image1.getCurrentZoom());
        image3.setMaxZoom(5);
        */
        num = 0;
        TouchImageView.activity = this;
        return view;
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.slide_left:
			leftFlip();
			break;
		case R.id.slide_right:
			rightFlip();
			break;
			
		case R.id.votebt_layout:
			parentActivity.setFragment1(1, "Vote");
			break;
		}
	}
	public void leftFlip(){
		if(num == 1){
	        votebt_layout.setVisibility(View.VISIBLE);
	        slide_layout_1.setVisibility(View.VISIBLE);
	        slide_layout_2.setVisibility(View.GONE);
	        slide_layout_3.setVisibility(View.GONE);
	        slide_left.setVisibility(View.GONE);
	        slide_right.setVisibility(View.VISIBLE);
	        animate(slide_layout_1, slide_layout_2, 1);
	        num--;
		}else if(num == 2){
	        votebt_layout.setVisibility(View.GONE);
	        slide_layout_1.setVisibility(View.GONE);
	        slide_layout_2.setVisibility(View.VISIBLE);
	        slide_layout_3.setVisibility(View.GONE);
	        slide_left.setVisibility(View.VISIBLE);
	        slide_right.setVisibility(View.VISIBLE);
	        animate(slide_layout_2, slide_layout_3, 1);
	        num--;
		}
	}
	public void rightFlip(){
		if(num == 0){
	        votebt_layout.setVisibility(View.GONE);
	        slide_layout_1.setVisibility(View.GONE);
	        slide_layout_2.setVisibility(View.VISIBLE);
	        slide_layout_3.setVisibility(View.GONE);
	        slide_left.setVisibility(View.VISIBLE);
	        slide_right.setVisibility(View.VISIBLE);
	        animate(slide_layout_2, slide_layout_1, 2);
	        num++;
		}else if(num == 1){
	        votebt_layout.setVisibility(View.GONE);
	        slide_layout_1.setVisibility(View.GONE);
	        slide_layout_2.setVisibility(View.GONE);
	        slide_layout_3.setVisibility(View.VISIBLE);
	        slide_left.setVisibility(View.VISIBLE);
	        slide_right.setVisibility(View.GONE);
	        animate(slide_layout_3, slide_layout_2, 2);
	        num++;
		}
	}
	public void animate(LinearLayout slide_layout, LinearLayout hidden_layout, int flg){
		if(flg == 1){
		    Animation animation1 = AnimationUtils.loadAnimation(view.getContext(), R.anim.push_right_out);
		    animation1.setDuration(500);
		    hidden_layout.startAnimation(animation1);
		    Animation animation = AnimationUtils.loadAnimation(view.getContext(), R.anim.push_left_in);
		    animation.setDuration(500);
		    slide_layout.startAnimation(animation);
		}else if(flg == 2){
		    Animation animation1 = AnimationUtils.loadAnimation(view.getContext(), R.anim.push_left_out);
		    animation1.setDuration(500);
		    hidden_layout.startAnimation(animation1);
		    Animation animation   =    AnimationUtils.loadAnimation(view.getContext(), R.anim.push_right_in);
		    animation.setDuration(500);
		    slide_layout.startAnimation(animation);
			
		}
	 }
	public class TouchLayoutListener implements OnTouchListener {
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			PointF curr = new PointF(event.getX(), event.getY());
	        
	            switch (event.getAction()) {
	                case MotionEvent.ACTION_DOWN:
	                	last.set(curr);
	                    break;
	                    
	                case MotionEvent.ACTION_MOVE:
	                    break;
	
	                case MotionEvent.ACTION_UP:
	                	if(curr.x - last.x > 150){
	                		leftFlip();
	                	}else if(curr.x - last.x < -150){
	                		rightFlip();
	                	}
	                case MotionEvent.ACTION_POINTER_UP:
	                    break;
	            }
	        return true;
		}
	}
}
