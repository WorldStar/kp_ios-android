package com.kidsperform.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

public class ReturnMessage {

	public static void showReturnCode(Context context, String string) {
		String msg = "";
		try{
		if (string.length() < 3) {
			showAlartDialog(context, "Fail, Please Try Again.");
			return;
		}
		switch (Integer.parseInt(string.substring(0, 3))) {
		case 000:
			break;
		case 999:
			String s = "REJECTED";
			String msg1 = "";
			int index =  0;
			if((index = string.indexOf(s)) != -1){
				msg1 = string.substring(index+s.length(),string.length());
			}
			msg = "Transaction Rejected. \n" + msg1;
			break;

		default:
			msg = "Server has problem right now. Please try again later.";
			break;
		}
		}catch(Exception e){
			msg = "Failed, Please Try Again.";
		}
		showAlartDialog(context, msg);

	}

	public static void showAlartDialog(Context context, String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg).setCancelable(false)
				.setNegativeButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	public static void showAlartDialog(Context context, String title, String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg).setCancelable(false).setTitle(title)
				.setNegativeButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void unableToLocateDialog(Context context) {
		new AlertDialog.Builder(context)
				.setTitle("Attention!")
				.setMessage(
						"Unable to locate your current location, please try again later.")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				}).show();
	}

	public static void confirmDialog(Activity activity, String title, String content) {
		new AlertDialog.Builder(activity)
				.setTitle(title)
				.setMessage(
						content)
				.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								//context.startActivity(new Intent(
										//Settings.ACTION_LOCATION_SOURCE_SETTINGS));
								
							}
						})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog4, int which) {
						// TODO Auto-generated method stub
						dialog4.cancel();
					}
				}).show();
	}
}
