package com.kidsperform.utils;

import com.kidsperform.VoteActivity;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

public class ViewPagerAdapter extends PagerAdapter {

	Activity activity;
	int imageArray[];
	MyButton slide_left;
	MyButton slide_right;
	LinearLayout votebt_layout;
	public ViewPagerAdapter(Activity act, int[] imgArra, MyButton slide_left, MyButton slide_right, LinearLayout votebt_layout) {
		imageArray = imgArra;
		activity = act;
		this.slide_left = slide_left;
		this.slide_right = slide_right;
		this.votebt_layout = votebt_layout;
	}

	@Override
	public int getCount() {
		return imageArray.length;
	}
	@Override
	public Object instantiateItem(View collection, int position) {
		ImageView view = new ImageView(activity);
		view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		view.setScaleType(ScaleType.FIT_XY);
		view.setBackgroundResource(imageArray[position]);
		((ViewPager) collection).addView(view, 0);
		VoteActivity.slidepos = position;
		if(position == 0){
			slide_left.setVisibility(View.GONE);
			slide_right.setVisibility(View.VISIBLE);
			votebt_layout.setVisibility(View.VISIBLE);
		}else if(position == 1){
			slide_left.setVisibility(View.VISIBLE);
			slide_right.setVisibility(View.VISIBLE);
			votebt_layout.setVisibility(View.GONE);
		}else if(position == 2){
			slide_left.setVisibility(View.VISIBLE);
			slide_right.setVisibility(View.GONE);
			votebt_layout.setVisibility(View.GONE);
		}
		return view;
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}

	@Override
	public Parcelable saveState() {
		return null;
	}
}
