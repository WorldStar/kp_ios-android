package com.kidsperform.utils;

import android.content.Context;
//import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

public class CustomViewPager extends ViewPager {

    private boolean enabled;
    Context con;
    public CustomViewPager(Context context, AttributeSet attrs) 
    {
        super(context, attrs);
        this.enabled = true;
        this.con = context;
    }


	@Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (this.enabled)
        {
        	/*if(event.getAction() == event.EDGE_LEFT){
        		setCurrentItem(currentitem - 1);
        	}else if(event.getAction() == event.EDGE_RIGHT){
            	setCurrentItem(currentitem + 1);
                
        	}*/        	
        	return super.onTouchEvent(event);
        }
  
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event)
    {
        if (this.enabled) 
        {
			 //Toast.makeText(con, "current position " + getCurrentItem(),
                     //Toast.LENGTH_SHORT).show();
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }
 
    public void setPagingEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }
    
    public boolean getEnable()
    {
    	return enabled;
    }
}