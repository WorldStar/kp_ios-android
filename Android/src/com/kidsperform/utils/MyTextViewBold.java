package com.kidsperform.utils;

import android.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class MyTextViewBold extends android.widget.TextView
{

    public MyTextViewBold(Context context)
    {
        this(context, null);
    }

    public MyTextViewBold(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public MyTextViewBold(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs);
        if (this.isInEditMode()) return ;
        this.setTypeface(
            Typeface.createFromAsset(context.getAssets(), "fonts/MYRIADPRO-REGULAR.OTF")
        );  
    }
}