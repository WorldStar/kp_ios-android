package com.kidsperform.lib;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Display;
import android.view.WindowManager;

public class ImageDecoder {

	public static Bitmap decodeBitmapForShowInList(Bitmap bitmap, Context context) {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		// save image with height about 15% height of a total screen
		return decodeBitmap(bitmap, (int) (display.getHeight() * 0.2));
	}

	public static Bitmap decodeBitmapForSave(Bitmap bitmap, Context context) {
		WindowManager wm = (WindowManager)  context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		// save image with height about 40% height of a total screen
		return decodeBitmap(bitmap, (int) (display.getHeight() * 0.4));
	}

	private static Bitmap decodeBitmap(Bitmap bitmap, int size) {

		// decode image size
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		if(stream.size()>0)
		{
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();

		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, o);

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = bitmap.getWidth(), height_tmp = bitmap.getHeight();
		int scale = 1;
		while (true) {
			// if(width_tmp/2<size || height_tmp/2<size)
			// only if the height less the preset height
			if (height_tmp / 2 < size)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}
		

		// decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory
				.decodeByteArray(byteArray, 0, byteArray.length, o2);

	}
		return bitmap;
		
	}
	}


