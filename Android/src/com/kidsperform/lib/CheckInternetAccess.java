package com.kidsperform.lib;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckInternetAccess {

	private Context context;
	
	public CheckInternetAccess (Context _context){
		this.context = _context;
		
	}
	
	public void createInternetDisabledAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(
				"Your internet network is disabled! Would you like to enable?")
				.setCancelable(false)
				.setPositiveButton("Enable internet",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								showInternetOptions();
							}
						});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void showInternetOptions() {
		Intent gpsOptionsIntent = new Intent(
				android.provider.Settings.ACTION_WIRELESS_SETTINGS);

		context.startActivity(gpsOptionsIntent);
	}
	
	public Boolean checkNetwork() {
		// get connection manager
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		// check if any network connection
		// use "netInfo == null && !netInfo.isConnectedOrConnecting()" will
		// crush when without network connection
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		} else {
			return false;
		}
	}
}
