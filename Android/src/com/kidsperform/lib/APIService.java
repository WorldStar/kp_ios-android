package com.kidsperform.lib;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;


public class APIService {
	String BASIC_URL = "http://imanage.asia/kp/ws/";
	String EVENT_URL = "voteevent.php";
	String ITEM_URL = "voteitem.php";
	String INFO_URL = "voteinfo.php";
	String RESULT_URL = "voteresult.php";
	
	public static String IMG_URL = "http://imanage.asia/kp/cms/";
	public static String BG_URL = "voteeventbackground/";
	public static String PHOTO_URL = "pic/";
	public static String HTML_URL = "pages/";
	String selected_url = "";
	String selected_img_url = "";
	public APIService(int i, int j){
		switch(i){
		case 1:
			selected_url = EVENT_URL;
			break;
		case 2:
			selected_url = ITEM_URL;
			break;
		case 3:
			selected_url = INFO_URL;
			break;
		case 4:
			selected_url = RESULT_URL;
			break;
			
		}
		switch(j){
		case 1:
			selected_img_url = BG_URL;
			break;
		case 2:
			selected_img_url = PHOTO_URL;
			break;
		case 3:
			selected_img_url = HTML_URL;
			break;
			
		}
	}
	public String getContentData(String params){
		String jsonObject = "";
		HttpGet httpGet = new HttpGet(BASIC_URL + selected_url +"?" + params);
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 50000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		try {
			jsonObject = stringBuilder.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}
	public static int convertDpToPixel(float dp, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    int px = (int)(dp * (metrics.densityDpi / 160f));
	    return px;
	}
}
