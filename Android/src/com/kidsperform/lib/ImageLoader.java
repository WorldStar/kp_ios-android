package com.kidsperform.lib;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageLoader {
	
	public String macc_no="";
	public String nname="";
	public String age="";
	public String date_dt="";
	public String venue="";
	public String status="";
	public String feedback_dt="";
	public String photo_url="";
	public Bitmap profile_photo;
	
	

	public static Bitmap getBitmapFromURL(String src)  {
	    try {
	        URL url = new URL(src);
	        //HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.setConnectTimeout(15000);
	        connection.setReadTimeout(15000);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inSampleSize = 1;
	        options.outHeight=600;
	        options.outWidth=480;
	        
	        Bitmap myBitmap = BitmapFactory.decodeStream(input,null,options);
	        
	        
	        
	        return myBitmap;
	    }catch (IOException e) {
	       
	        return null;
	    }catch(OutOfMemoryError error){
	        return null;
	    }
	    
	   
	}
	public static Bitmap decodeFile(String src){
	    try {
	        //Decode image size
	    	URL url = new URL(src);
	        //HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
	    	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.setConnectTimeout(15000);
	        connection.setReadTimeout(15000);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(input,null,o);

	        //The new size we want to scale to
	        final int REQUIRED_SIZE=150;

	        //Find the correct scale value. It should be the power of 2.
	        int scale=1;
	        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
	            scale*=2;

	        //Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        Bitmap mybitmap = BitmapFactory.decodeStream(input, null, o2);
	        input.close();
	        connection.disconnect();    	
	        return mybitmap;
	    } catch (Exception e) {
	    	return null;
	    }catch(OutOfMemoryError error){
	    	System.gc();
	        return null;
	    }
	}

}
