package com.kidsperform.lib;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public abstract interface DiskCache
{
  public abstract void cleanup();

  public abstract void clear();

  public abstract boolean exists(String paramString);

  public abstract File getFile(String paramString);

  public abstract InputStream getInputStream(String paramString)
    throws IOException;

  public abstract void invalidate(String paramString);

  public abstract void store(String paramString, InputStream paramInputStream);
}