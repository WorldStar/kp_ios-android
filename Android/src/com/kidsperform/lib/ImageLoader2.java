package com.kidsperform.lib;

import java.io.File;
import java.util.HashMap;
import java.util.Stack;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.kidsperform.R;

public class ImageLoader2
{	
	public static final int SINGLE_IMAGE = 100;
	public static final int LIST_IMAGE = 101;
	public static final int GALLERY_IMAGE = 102;
	int iImageStatus;

	// the simplest in-memory cache implementation. This should be replaced with
	// something like SoftReference or BitmapOptions.inPurgeable(since 1.6)
	private HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();
	private Context context;
	
	public ImageLoader2(Context context, int status)
	{
		iImageStatus = status;
		// Make the background thead low priority. This way it will not affect
		// the UI performance
		photoLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);
		this.context = context;		
	}

	int stub_id = R.drawable.icon_user;

	public void DisplayImage(String url, Context context, ImageView imageView,ProgressBar pb) 
	{
		//url = url.replace(" ", "%20");

		this.context = context;
		/*if (cache.containsKey(url))
		{
			//Bitmap tmp = cache.get(url);
			imageView.setImageBitmap(cache.get(url));
			//tmp.recycle();
		}
		else if(cache == null)
		{
			imageView.setImageResource(stub_id);
		}
		else 
		{*/
			// Log.d("queing", "....");
			queuePhoto(url, context, imageView, pb);
			imageView.setImageResource(stub_id);
		//}
	}
	public void DisplayImage2(String url, Context context, ImageView imageView,ProgressBar pb, int defaultimg) 
	{
		//url = url.replace(" ", "%20");
		this.stub_id = defaultimg;
		this.context = context;
		if (cache.containsKey(url))
		{
			//Bitmap tmp = cache.get(url);
			imageView.setImageBitmap(Bitmap.createScaledBitmap(cache.get(url), 120, 120, false));
			//tmp.recycle();
		}
		else if(cache == null)
		{
			imageView.setImageResource(stub_id);
		}
		else 
		{
			queuePhoto(url, context, imageView, pb);
			imageView.setImageResource(stub_id);
		}
	}
	public void DisplayImage4(String url, Context context, ImageView imageView,ProgressBar pb, int defaultimg) 
	{
		//url = url.replace(" ", "%20");
		this.stub_id = defaultimg;
		this.context = context;
		
		if (cache.containsKey(url))
		{
			//Bitmap tmp = cache.get(url);
			imageView.setImageBitmap(Bitmap.createScaledBitmap(cache.get(url), 300, 300, false));
			//tmp.recycle();
		}
		else if(cache == null)
		{
			imageView.setImageResource(stub_id);
		}
		else 
		{
			queuePhoto(url, context, imageView, pb);
			imageView.setImageResource(stub_id);
		}
	}
	public void DisplayImage3(String url, Context context, ImageView imageView,ProgressBar pb, int defaultimg) 
	{
		//url = url.replace(" ", "%20");
		this.stub_id = defaultimg;
		this.context = context;
			queuePhoto(url, context, imageView, pb);
			imageView.setImageResource(stub_id);
	}
	public void DisplayImage1(String url, Context context, ImageView imageView,ProgressBar pb) 
	{
		//url = url.replace(" ", "%20");

		this.context = context;
		if (cache.containsKey(url))
		{
			//Bitmap tmp = cache.get(url);
			imageView.setImageBitmap(Bitmap.createScaledBitmap(cache.get(url), 120, 120, false));
			//tmp.recycle();
		}
		else if(cache == null)
		{
			imageView.setImageResource(stub_id);
		}
		else 
		{
			queuePhoto(url, context, imageView, pb);
			imageView.setImageResource(stub_id);
		}
	}

	private void queuePhoto(String url, Context context, ImageView imageView,ProgressBar pb) 
	{
		// This ImageView may be used for other images before. So there may be
		// some old tasks in the queue. We need to discard them.
		photosQueue.Clean(imageView);
		PhotoToLoad p = new PhotoToLoad(url, imageView, pb);
		synchronized (photosQueue.photosToLoad) 
		{
			photosQueue.photosToLoad.push(p);
			photosQueue.photosToLoad.notifyAll();
		}
		// start thread if it's not started yet
		//if (photoLoaderThread.getState() == Thread.State.NEW)
		if(!photoLoaderThread.isAlive())
			photoLoaderThread.start();
			
	}

	// Task for the queue
	private class PhotoToLoad
	{
		public String url;
		public ImageView imageView;
		public ProgressBar progressBar;

		public PhotoToLoad(String u, ImageView i, ProgressBar pb)
		{
			url = u;
			imageView = i;
			progressBar = pb;
		}
	}

	PhotosQueue photosQueue = new PhotosQueue();

	public void stopThread() 
	{
		photoLoaderThread.interrupt();
	}

	// stores list of photos to download
	class PhotosQueue 
	{
		private Stack<PhotoToLoad> photosToLoad = new Stack<PhotoToLoad>();

		// removes all instances of this ImageView
		public void Clean(ImageView image) 
		{
			for (int j = 0; j < photosToLoad.size();) 
			{
				if (photosToLoad.get(j).imageView == image)
				{
					photosToLoad.remove(j);
				}
				else
					{
					++j;
					}
			}
		}
	}

	class PhotosLoader extends Thread
	{
		public void run() {
			try {
				while (true) {
					// thread waits until there are any images to load in the
					// queue
					if (photosQueue.photosToLoad.size() == 0)
						synchronized (photosQueue.photosToLoad) {
							photosQueue.photosToLoad.wait();
						}
					if (photosQueue.photosToLoad.size() != 0) {
						PhotoToLoad photoToLoad;
						synchronized (photosQueue.photosToLoad) {
							photoToLoad = photosQueue.photosToLoad.pop();
						}
					
						
						Bitmap bmp = null;
						ImageHandler imageHandler = new ImageHandler(context);
								
						bmp = imageHandler.getPublicBitmap(photoToLoad.url);
												
						if (bmp != null) 
						{
							if (iImageStatus == LIST_IMAGE)
							{
								bmp = ImageDecoder.decodeBitmapForShowInList( bmp, context);
							}
							//cache.put(photoToLoad.url, bmp);
							// Log.d("cache size", cache.)
							Object tag = photoToLoad.imageView.getTag();
							if (tag != null
									&& ((String) tag).equals(photoToLoad.url)) {
								BitmapDisplayer bd = new BitmapDisplayer(bmp,
										photoToLoad.imageView,
										photoToLoad.progressBar);
								Activity a = (Activity) photoToLoad.imageView
										.getContext();
								a.runOnUiThread(bd);
							}
						}
						
											}
					if (Thread.interrupted())
						break;
				}
			} catch (InterruptedException e) {
				// allow thread to exit
			}
		}
	}

	PhotosLoader photoLoaderThread = new PhotosLoader();

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		ImageView imageView;
		ProgressBar progressBar;

		public BitmapDisplayer(Bitmap b, ImageView i, ProgressBar pb) {
			bitmap = b;
			imageView = i;
			progressBar = pb;
		}

		public void run() {
			if (bitmap == null) {
				imageView.setImageResource(stub_id);
				
			} else {
				imageView.setImageBitmap(bitmap);
				if (progressBar != null)
					progressBar.setVisibility(View.GONE);
			}
		}
	}
	public void clearCache() {
		// clear memory cache
		//cache.clear();
		File[] internalCacheFile = context.getCacheDir().listFiles();
		for (File f : internalCacheFile)
			f.delete();

		File[] privatefiles = context.getFilesDir().listFiles();
		for (File f : privatefiles)
			f.delete();
	}
}
