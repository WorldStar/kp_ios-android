package com.kidsperform.lib;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.kidsperform.R;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;


public class ImageHandler {
	/*
	 * External storage (like SD card) is the priority selection storage if the
	 * device have external storage we should save the necessary file in
	 * external storage else, save in internal storage
	 * 
	 * Save file in CacheDir have few benefit: 1. Image wont show in photo
	 * gallery 2. When user uninstall the app, the file in CacheDir will be
	 * clean up by system
	 * 
	 * For the important or private file need to store in the Internal Private
	 * storage In this situation is user QR code and voucher QR code In Internal
	 * Private storage either user nor application was unable to access the file
	 * the only application can access is this app, it will delete when the app
	 * uninstall too
	 */

	private File publicDir;
	private Context context;

	public ImageHandler(Context context) {
		this.context = context;

		// Find the dir to save cached images
		// save in cache will be delete when user delete the app and also
		// prevent the image show in gellery
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			publicDir = context.getExternalCacheDir();
		} else {
			publicDir = context.getCacheDir();
		}

//		if (!publicDir.exists()) {
//			publicDir.mkdirs();
//		}
//		Log.d("publicDir@image handler", publicDir.getAbsolutePath());
	}

	public void SavePrivateImage(Bitmap pic, String sName) {
		FileOutputStream fos;
		try {
			fos = context.openFileOutput(sName, Context.MODE_PRIVATE);
			pic.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void SavePrivateImage(String url) {
		url = url.replace(" ", "%20");

		Bitmap bitmap = DownloadBitmap(url);

		FileOutputStream fos;
		try {
			fos = context.openFileOutput(getQRNameFromUrl(url),
					Context.MODE_PRIVATE);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String SavePublicImage(Bitmap pic, String sName) {
		File fPhoto;
		String fPhotoPath = "";

		try {
			fPhoto = new File(publicDir, sName);
			fPhotoPath = fPhoto.toURL().toString();

			// save the image to public storage
			FileOutputStream fos = new FileOutputStream(fPhoto);
			pic.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fPhotoPath;

	}

	public String SavePublicImage(String url) {
		url = url.replace(" ", "%20");

		Bitmap bitmap = DownloadBitmap(url);

		File fPhoto;
		String fPhotoPath = "";

		try {
			fPhoto = new File(publicDir, getNameFromUrl(url));
			fPhotoPath = fPhoto.toURL().toString();

			// save the image to public storage
			FileOutputStream fos = new FileOutputStream(fPhoto);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
			bitmap.recycle();
			fos.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fPhotoPath;

	}

	public Bitmap DownloadBitmap(String url) {
		Bitmap bitmapFromUrl = null;
		URL newurl;

		try {
			Log.d("Downloading", url);
			newurl = new URL(url);

			HttpGet httpRequest = null;
			try {
				httpRequest = new HttpGet(newurl.toURI());
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response = (HttpResponse) httpclient
						.execute(httpRequest);
				HttpEntity entity = response.getEntity();
				BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
						entity);
				InputStream instream = bufHttpEntity.getContent();
				
				bitmapFromUrl = BitmapFactory.decodeStream(instream);
				instream.close();

			} catch (URISyntaxException e) {
				Log.d("Download Error URL", url);
				bitmapFromUrl = BitmapFactory.decodeResource(
						context.getResources(), R.drawable.icon_user);
				e.printStackTrace();
			} catch (Exception e) {
				Log.d("Download Error Exception", url);
				bitmapFromUrl = BitmapFactory.decodeResource(
						context.getResources(), R.drawable.icon_user);
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return bitmapFromUrl;
	}

	public static String getNameFromUrl(String url) {
		String filename[] = url.split("[/\\.]");
		String name = filename[filename.length - 2];
		return name;
	}

	public static String getQRNameFromUrl(String url) {
		String filename[] = url.split("[=]");
		String name = filename[filename.length - 1];
		return name;
	}

	public Bitmap getPublicBitmap(String url) {
		if (url.length() > 0) {
			// replace space from url to %20 to avoid download fail
			url = url.replace(" ", "%20");

			String imageName = getNameFromUrl(url);

			Log.d("ImageName@getPublicBitmap@ImageHandler", imageName);

			// try to get image from public dir
			File fileName = new File(publicDir, imageName);

			FileInputStream fis = null;

			try {
				fis = new FileInputStream(fileName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			Bitmap bitmapFromPublicDir = null;
			try{
			   bitmapFromPublicDir = BitmapFactory.decodeStream(fis, null,
					null);
			}catch(OutOfMemoryError e){
				
			}
			// if get, return
			// else download from internet
			if (bitmapFromPublicDir != null) {
				return bitmapFromPublicDir;
			} else {
				try{
				  Bitmap bitmapFromUrl = DownloadBitmap(url);
					// decode to smaller size before store
					Bitmap bitmap = ImageDecoder.decodeBitmapForSave(bitmapFromUrl,
							context);
					SavePublicImage(bitmap, imageName);
					if (bitmap != null) {
						return bitmap;
					}
				}catch(OutOfMemoryError e){
					return null;
				}

			}
		}

		return null;
	}

	public Bitmap getPrivateBitmap(String url) {

		if (url.length() > 0) {

			// replace space from url to %20 to avoid download fail
			url = url.replace(" ", "%20");

			String imageName = getQRNameFromUrl(url);

			Log.d("Private Image Name", imageName);

			FileInputStream fis = null;

			try {
				fis = context.openFileInput(imageName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			Bitmap bitmapFromPublicDir = BitmapFactory.decodeStream(fis, null,
					null);

			// if get, return
			// else download from internet
			if (bitmapFromPublicDir != null) {
				Log.d(imageName, "no download...");
				return bitmapFromPublicDir;
			} else {
				Log.d(imageName, "download...");
				Bitmap bitmapFromUrl = DownloadBitmap(url);

				// decode to smaller size before store
				Bitmap bitmap = ImageDecoder.decodeBitmapForSave(bitmapFromUrl,
						context);
				SavePrivateImage(bitmap, imageName);
				return bitmap;
			}
		}

		return null;
	}
	public Bitmap getPrivateBitmap1(String url) {

		if (url.length() > 0) {

			// replace space from url to %20 to avoid download fail
			url = url.replace(" ", "%20");
			//url = "http://moola.sim.ingenuity.com.my//ShowQRVoucher.ashx?serialNumber=3FE105704B334973AB092DF9DEAF22B8";
			String imageName = getQRNameFromUrl(url);

			
				Bitmap bitmapFromUrl = DownloadBitmap(url);

				// decode to smaller size before store
				Bitmap bitmap = ImageDecoder.decodeBitmapForSave(bitmapFromUrl,
						context);
				//SavePrivateImage(bitmap, imageName);
				return bitmap;
		}

		return null;
	}

	public void clearCache() {

		// clear public cache
//		if (Environment.getExternalStorageState().equals(
//				Environment.MEDIA_MOUNTED)) {
//			File[] externalCacheFile = context.getExternalCacheDir().;
//			for (File f : externalCacheFile)
//				f.delete();
//		}

		File[] internalCacheFile = context.getCacheDir().listFiles();
		for (File f : internalCacheFile)
			f.delete();

		File[] privatefiles = context.getFilesDir().listFiles();
		for (File f : privatefiles)
			f.delete();

	}

}
