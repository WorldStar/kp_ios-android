package com.kidsperform;


import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.kidsperform.BillingService.RequestPurchase;
import com.kidsperform.BillingService.RestoreTransactions;
import com.kidsperform.Consts.PurchaseState;
import com.kidsperform.Consts.ResponseCode;
import com.kidsperform.MainActivity.MenuAdapter;
import com.kidsperform.VoteItemActivity.VoteItemAdapter;
import com.kidsperform.VotePurchaseActivity.DungeonsPurchaseObserver;
import com.kidsperform.data.MenuList;
import com.kidsperform.data.VoteEList;
import com.kidsperform.data.VoteInfoList;
import com.kidsperform.data.VoteItemList;
import com.kidsperform.lib.APIService;
import com.kidsperform.lib.CheckInternetAccess;
import com.kidsperform.lib.ImageLoader;
import com.kidsperform.lib.ImageLoader2;
import com.kidsperform.lib.ImageLoader4;
import com.kidsperform.lib.JsonHelper;
import com.kidsperform.utils.MyTextView;
import com.kidsperform.utils.ReturnMessage;
import com.kidsperform.utils.Util;
import com.kidsperform.R;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;
import com.kidsperform.R.string;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

public class VoteDetailsActivity extends Fragment implements OnClickListener{
	protected MainActivity parentActivity;
	VoteItemList voteitemlist = new VoteItemList();
	VoteInfoList voteinfolist = new VoteInfoList();
	View view;
	MyTextView nametx, percenttx, desctx, termstx, freetx, categorytx;
	LinearLayout percent_layout, votebt, scroll_layout;
	ImageView imageview;
	EditText nameEdit, remarkEdit;
	String terms = "";
	

    private DungeonsPurchaseObserver mDungeonsPurchaseObserver;
    private Handler mHandler;

    private BillingService mBillingService;
    private PurchaseDatabase mPurchaseDatabase;
    String mailaddress = "";
    private static final String TAG = "KipPerform";
    private static final int DIALOG_CANNOT_CONNECT_ID = 1;
    private static final int DIALOG_BILLING_NOT_SUPPORTED_ID = 2;
    private static final int DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID = 3;
    private String mItemName;
    private String mSku;
    private Managed mManagedType;
    private CatalogAdapter mCatalogAdapter;
    private enum Managed { MANAGED, UNMANAGED, SUBSCRIPTION }
    private Set<String> mOwnedItems = new HashSet<String>();
    private static final String DB_INITIALIZED = "db_initialized";
    private String mPayloadContents = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.vote_details, null);
        parentActivity = (MainActivity)getActivity();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        voteitemlist = parentActivity.selecteditemlist;
        nametx = (MyTextView)view.findViewById(R.id.nametx);
        categorytx = (MyTextView)view.findViewById(R.id.categorytx);
        percenttx = (MyTextView)view.findViewById(R.id.percenttx);
        desctx = (MyTextView)view.findViewById(R.id.desctx);
        termstx = (MyTextView)view.findViewById(R.id.termstx);
        freetx = (MyTextView)view.findViewById(R.id.freetx);
        nameEdit = (EditText)view.findViewById(R.id.nameEdit);
        remarkEdit = (EditText)view.findViewById(R.id.remarkEdit);
        nameEdit.setTypeface(Typeface.createFromAsset(parentActivity.getAssets(), "fonts/MYRIADPRO-REGULAR.OTF"));
        remarkEdit.setTypeface(Typeface.createFromAsset(parentActivity.getAssets(), "fonts/MYRIADPRO-REGULAR.OTF"));
        SharedPreferences prefs = parentActivity.getPreferences(parentActivity.MODE_PRIVATE);
        String votename = prefs.getString("votename", "");
        String votemail = prefs.getString("votemail", "");
        nameEdit.setText(votename);
        remarkEdit.setText(votemail);
        ((ScrollView)view.findViewById(R.id.scrollView1)).pageScroll(View.FOCUS_UP);
        scroll_layout = (LinearLayout)view.findViewById(R.id.scroll_layout);
        float scale = getResources().getDisplayMetrics().density;
        //int th = (int)(newBitmap.getHeight() * 3 / 4);
        //if(th > parentActivity.height * 2 / 3){
        int th = parentActivity.height  / 2;
        //}
        scroll_layout.setPadding(0, th, 0, 0);
        //nameEdit.requestFocus();
        //nameEdit.setFocusableInTouchMode(true);
        //InputMethodManager imm = (InputMethodManager) parentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.showSoftInput(nameEdit, InputMethodManager.SHOW_IMPLICIT);
        
        imageview = (ImageView)view.findViewById(R.id.ImageView1);
        //new GetBGTask().execute();
        
        
        percent_layout = (LinearLayout)view.findViewById(R.id.percent_layout);
        votebt = (LinearLayout)view.findViewById(R.id.votebt);
        votebt.setOnClickListener(this);
        
        nametx.setText(voteitemlist.name);
        categorytx.setText(parentActivity.selectednowlist.title);
        percenttx.setText(voteitemlist.pvalue+"%");
        desctx.setText(voteitemlist.desc);
        freetx.setText("(free vote: "+parentActivity.selectednowlist.freecount+")");
        int val = (int)((float)(120) * voteitemlist.percent / 100);
        int px1 = APIService.convertDpToPixel(val, parentActivity);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(px1, 8);
		percent_layout.setLayoutParams(params);
		((ScrollView)view.findViewById(R.id.scrollView1)).scrollTo(0, 0);
		
		mHandler = new Handler();
        mDungeonsPurchaseObserver = new DungeonsPurchaseObserver(mHandler);
        mBillingService = new BillingService();
        mBillingService.setContext(parentActivity);
        //AccountManager am = AccountManager.get(parentActivity);
        //Account[] accounts = am.getAccountsByType("com.google"); 
        
        //Account mainaccount = accounts[0];
        //mailaddress = mainaccount.name;
        mPurchaseDatabase = new PurchaseDatabase(parentActivity);
        
        ResponseHandler.register(mDungeonsPurchaseObserver);
        if (!mBillingService.checkBillingSupported()) {
        	parentActivity.showDialog(DIALOG_CANNOT_CONNECT_ID);
        }
        
        if (!mBillingService.checkBillingSupported(Consts.ITEM_TYPE_SUBSCRIPTION)) {
        	parentActivity.showDialog(DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID);
        }
        mItemName = getString(CATALOG[0].nameId);
        mSku = CATALOG[0].sku;
        mManagedType = CATALOG[0].managed;
		
        return view;
    }
    private void restoreDatabase() {
        SharedPreferences prefs = parentActivity.getPreferences(parentActivity.MODE_PRIVATE);
        boolean initialized = prefs.getBoolean(DB_INITIALIZED, false);
        if (!initialized) {
            mBillingService.restoreTransactions();
            Toast.makeText(parentActivity, R.string.restoring_transactions, Toast.LENGTH_LONG).show();
        }
    }
    public class DungeonsPurchaseObserver extends PurchaseObserver {
        public DungeonsPurchaseObserver(Handler handler) {
            super(parentActivity, handler);
        }

        @Override
        public void onBillingSupported(boolean supported, String type) {
            if (Consts.DEBUG) {
                Log.i(TAG, "supported: " + supported);
            }
            if (type == null || type.equals(Consts.ITEM_TYPE_INAPP)) {
                if (supported) {
                    restoreDatabase();
                	votebt.setEnabled(true);
                } else {
                	parentActivity.showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
                }
            } else if (type.equals(Consts.ITEM_TYPE_SUBSCRIPTION)) {
                //mCatalogAdapter.setSubscriptionsSupported(supported);
            } else {
            	parentActivity.showDialog(DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID);
            }
        }

        @Override
        public void onPurchaseStateChange(PurchaseState purchaseState, String itemId,
                int quantity, long purchaseTime, String developerPayload) {
            if (Consts.DEBUG) {
                Log.i(TAG, "onPurchaseStateChange() itemId: " + itemId + " " + purchaseState);
            }

            if (developerPayload == null) {
                //logProductActivity(itemId, purchaseState.toString());
            } else {
                //logProductActivity(itemId, purchaseState + "\n\t" + developerPayload);
            }
            
            if (purchaseState == PurchaseState.PURCHASED) {
                mOwnedItems.add(itemId);
                
                // If this is a subscription, then enable the "Edit
                // Subscriptions" button.
                for (CatalogEntry e : CATALOG) {
                    if (e.sku.equals(itemId) &&
                            e.managed.equals(Managed.SUBSCRIPTION)) {
                        //mEditSubscriptionsButton.setVisibility(View.VISIBLE);
                    }
                }
            }
            //mCatalogAdapter.setOwnedItems(mOwnedItems);
            //mOwnedItemsCursor.requery();
        }

        @Override
        public void onRequestPurchaseResponse(RequestPurchase request,
                ResponseCode responseCode) {

        	//ReturnMessage.showAlartDialog(parentActivity, request.toString()+":"+responseCode);
            //Toast.makeText(parentActivity, "received", 500);
        	if (Consts.DEBUG) {
                Log.d(TAG, request.mProductId + ": " + responseCode);
            }
            if (responseCode == ResponseCode.RESULT_OK) {
                if (Consts.DEBUG) {
                    Log.i(TAG, "purchase was successfully sent to server");
                }
               // Toast.makeText(parentActivity, "OKOK", 500);
                //new GetConfirmTask().execute();
                logProductActivity(request.mProductId, "sending purchase request");
            } else if (responseCode == ResponseCode.RESULT_USER_CANCELED) {
                if (Consts.DEBUG) {
                    Log.i(TAG, "user canceled purchase");
                }
                //ReturnMessage.showAlartDialog(parentActivity, "dismissed purchase dialog");
                //logProductActivity(request.mProductId, "dismissed purchase dialog");
            } else {
                if (Consts.DEBUG) {
                    Log.i(TAG, "purchase failed");
                }
                //ReturnMessage.showAlartDialog(parentActivity, "request purchase returned " + responseCode);
                //logProductActivity(request.mProductId, "request purchase returned " + responseCode);
            }
        }

        @Override
        public void onRestoreTransactionsResponse(RestoreTransactions request,
                ResponseCode responseCode) {
        	//ReturnMessage.showAlartDialog(parentActivity, request.toString()+":"+responseCode);
            if (responseCode == ResponseCode.RESULT_OK) {
                if (Consts.DEBUG) {
                    Log.d(TAG, "completed RestoreTransactions request");
                }
                // Update the shared preferences so that we don't perform
                // a RestoreTransactions again.
                SharedPreferences prefs = parentActivity.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(DB_INITIALIZED, true);
                edit.commit();
            } else {
                if (Consts.DEBUG) {
                    Log.d(TAG, "RestoreTransactions error: " + responseCode);
                }
            }
        }
    }

    private void logProductActivity(String product, String activity) {
        /*SpannableStringBuilder contents = new SpannableStringBuilder();
        contents.append(Html.fromHtml("<b>" + product + "</b>: "));
        contents.append(activity);
        prependLogEntry(contents);
        */
    	new GetConfirmPayTask().execute();
    }
    /** An array of product list entries for the products that can be purchased. */
    private static final CatalogEntry[] CATALOG = new CatalogEntry[] {
        new CatalogEntry("vote", R.string.two_handed_sword, Managed.UNMANAGED),
        /*new CatalogEntry("potion_001", R.string.potions, Managed.UNMANAGED),
        new CatalogEntry("subscription_monthly", R.string.subscription_monthly,
                Managed.SUBSCRIPTION),
        new CatalogEntry("subscription_yearly", R.string.subscription_yearly,
                Managed.SUBSCRIPTION),
        new CatalogEntry("android.test.purchased", R.string.android_test_purchased,
                Managed.UNMANAGED),
        new CatalogEntry("android.test.canceled", R.string.android_test_canceled,
                Managed.UNMANAGED),
        new CatalogEntry("android.test.refunded", R.string.android_test_refunded,
                Managed.UNMANAGED),
        new CatalogEntry("android.test.item_unavailable", R.string.android_test_item_unavailable,
                Managed.UNMANAGED),*/
    };
    private static class CatalogEntry {
        public String sku;
        public int nameId;
        public Managed managed;

        public CatalogEntry(String sku, int nameId, Managed managed) {
            this.sku = sku;
            this.nameId = nameId;
            this.managed = managed;
        }
    }
    private static class CatalogAdapter extends ArrayAdapter<String> {
        private CatalogEntry[] mCatalog;
        private Set<String> mOwnedItems = new HashSet<String>();
        private boolean mIsSubscriptionsSupported = false;

        public CatalogAdapter(Context context, CatalogEntry[] catalog) {
            super(context, android.R.layout.simple_spinner_item);
            mCatalog = catalog;
            for (CatalogEntry element : catalog) {
                add(context.getString(element.nameId));
            }
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }

        public void setOwnedItems(Set<String> ownedItems) {
            mOwnedItems = ownedItems;
            notifyDataSetChanged();
        }

        public void setSubscriptionsSupported(boolean supported) {
            mIsSubscriptionsSupported = supported;
        }

        @Override
        public boolean areAllItemsEnabled() {
            // Return false to have the adapter call isEnabled()
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            // If the item at the given list position is not purchasable,
            // then prevent the list item from being selected.
            CatalogEntry entry = mCatalog[position];
            if (entry.managed == Managed.MANAGED && mOwnedItems.contains(entry.sku)) {
                return false;
            }
            if (entry.managed == Managed.SUBSCRIPTION && !mIsSubscriptionsSupported) {
                return false;
            }
            return true;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            // If the item at the given list position is not purchasable, then
            // "gray out" the list item.
            View view = super.getDropDownView(position, convertView, parent);
            view.setEnabled(isEnabled(position));
            return view;
        }
    }


    @Override
	public void onStart() {
        super.onStart();
        ResponseHandler.register(mDungeonsPurchaseObserver);
        System.gc();
        if(parentActivity.selecteditemlist.bitmap != null){
			//BitmapDrawable background = new BitmapDrawable(bgmap);
			float x = parentActivity.selecteditemlist.bitmap.getWidth();
			float y = parentActivity.selecteditemlist.bitmap.getHeight();
			int he = (int)(parentActivity.width * (  y / x));
			RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(parentActivity.width, he);
			imageview.setLayoutParams(layoutParams);
			
			//Bitmap bitmap = background.getBitmap();
			//float scalingFactor = getBitmapScalingFactor(bitmap);
			//Bitmap newBitmap = Util.ScaleBitmap(bitmap, scalingFactor);
			
			//imageview.setImageDrawable(background);
			//imageview.setImageDrawable(background);
			imageview.setImageBitmap(parentActivity.selecteditemlist.bitmap);
			//newBitmap = null;
			//bgmap.recycle();

			if(parentActivity.selecteditemlist.bitmap != null){
				parentActivity.selecteditemlist.bitmap = null;
			}
			
		}
		new GetInfoTask().execute();
		((ScrollView)view.findViewById(R.id.scrollView1)).scrollTo(0, 0);
        //initializeOwnedItems();
    }
    @Override
	public void onStop() {
        super.onStop();
        ResponseHandler.unregister(mDungeonsPurchaseObserver);
        System.gc();
    }
    @Override
	public void onDestroy() {
        super.onDestroy();
        mPurchaseDatabase.close();
        mBillingService.unbind();
        imageview = null;
        
        System.gc();
    }
    private void showPayloadEditDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(parentActivity);
        final View view = View.inflate(parentActivity, R.layout.edit_payload, null);
        final TextView payloadText = (TextView) view.findViewById(R.id.payload_text);
        if (mPayloadContents != null) {
            payloadText.setText(mPayloadContents);
        }

        dialog.setView(view);
        dialog.setPositiveButton(
                R.string.edit_payload_accept,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPayloadContents = payloadText.getText().toString();
                    }
                });
        dialog.setNegativeButton(
                R.string.edit_payload_clear,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialog != null) {
                            mPayloadContents = null;
                            dialog.cancel();
                        }
                    }
                });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (dialog != null) {
                    dialog.cancel();
                }
            }
        });
        dialog.show();
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.votebt){
			if(nameEdit.getText().toString().equals("")){
				ReturnMessage.showAlartDialog(parentActivity, "Please enter your name.");
				nameEdit.setFocusable(true);
				nameEdit.requestFocus();
				return;
			}
			if(remarkEdit.getText().toString().equals("")){
				ReturnMessage.showAlartDialog(parentActivity, "Please enter your email.");
				remarkEdit.setFocusable(true);
				remarkEdit.requestFocus();
				return;
			}
			String na = nameEdit.getText().toString();
			na = na.replace(" ", "%20");
			parentActivity.selecteditemlist.pname = na;//nameEdit.getText().toString();
			String a = remarkEdit.getText().toString();
			/*byte[] bute = null;
	        bute = a.getBytes();
	        for(int i = 0; i < bute.length; i++){
	        	if(bute[i] == 10) bute[i] = 32;
	        }
	        String re = "";
	        try {
	            re= new String(bute, "UTF-8");
	            
	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            re = "";
	        }
	        */

			a = a.replace(" ", "%20");
			parentActivity.selecteditemlist.premark = a;
			SharedPreferences prefs = parentActivity.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString("votename", na);
            edit.putString("votemail", a);
            edit.commit();
            InputMethodManager imm = (InputMethodManager)parentActivity.getSystemService(
          	      Context.INPUT_METHOD_SERVICE);
          	imm.hideSoftInputFromWindow(nameEdit.getWindowToken(), 0);
          	imm.hideSoftInputFromWindow(remarkEdit.getWindowToken(), 0);
			if(parentActivity.selectednowlist.checkfree == 0){
				new GetConfirmTask().execute();
			}else{
				//parentActivity.setFragment(3, "Confirm");
				//ReturnMessage.showAlartDialog(parentActivity, "Processing for purchase. Now pending.");
				//Intent purchaseintent = new Intent(parentActivity, Dungeons.class);
				//parentActivity.startActivity(purchaseintent);
				AccountManager am = AccountManager.get(parentActivity);
		        Account[] accounts = am.getAccountsByType("com.google"); 
		        if(accounts.length == 0){
		        	ReturnMessage.showAlartDialog(parentActivity, "Please log in on play store app and try again.");
		        	return;
		        }
		        Account mainaccount = accounts[0];
		        mailaddress = mainaccount.name;
				//parentActivity.setFragment(3, parentActivity.selectednowlist.title);
				//if (Consts.DEBUG) {
	               // Log.d(TAG, "buying: " + mItemName + " sku: " + mSku);
	            //}

	            //if (mManagedType != Managed.SUBSCRIPTION &&
	                   // !mBillingService.requestPurchase(mSku, Consts.ITEM_TYPE_INAPP, mPayloadContents)) {
	            	//parentActivity.showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
	            //} else 
	            	if (!mBillingService.requestPurchase(mSku, Consts.ITEM_TYPE_SUBSCRIPTION, mPayloadContents)) {
	                // Note: mManagedType == Managed.SUBSCRIPTION
	            	parentActivity.showDialog(DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID);
	            }
			}
		}
	}

    private float getBitmapScalingFactor(Bitmap bm) {
        // Get display width from device
        int displayWidth = parentActivity.width;

        // Get margin to use it for calculating to max width of the ImageView
        RelativeLayout.LayoutParams layoutParams = 
            (RelativeLayout.LayoutParams)imageview.getLayoutParams();
        int leftMargin = layoutParams.leftMargin;
        int rightMargin = layoutParams.rightMargin;

        // Calculate the max width of the imageView
        int imageViewWidth = displayWidth - (leftMargin + rightMargin);

        // Calculate scaling factor and return it
        return ( (float) imageViewWidth / (float) bm.getWidth() );
    }
	private class GetBGTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
		Bitmap bgmap = null;
       public GetBGTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(bgmap != null){
				//BitmapDrawable background = new BitmapDrawable(bgmap);
				float x = bgmap.getWidth();
				float y = bgmap.getHeight();
				int he = (int)(parentActivity.width * (  y / x));
				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(parentActivity.width, he);
				imageview.setLayoutParams(layoutParams);
				
				//Bitmap bitmap = background.getBitmap();
				//float scalingFactor = getBitmapScalingFactor(bitmap);
				//Bitmap newBitmap = Util.ScaleBitmap(bitmap, scalingFactor);
				
				//imageview.setImageDrawable(background);
				//imageview.setImageDrawable(background);
				imageview.setImageBitmap(bgmap);
				//newBitmap = null;
				//bgmap.recycle();

				if(bgmap != null){
					bgmap = null;
				}
				
			}
			new GetInfoTask().execute();
			((ScrollView)view.findViewById(R.id.scrollView1)).scrollTo(0, 0);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(parentActivity, "",
	                    "Loading background image... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
	        	if(voteitemlist.imgurl != null){
	        		String imgurl = APIService.IMG_URL+APIService.PHOTO_URL+voteitemlist.imgurl;
	        		imageview.setTag(imgurl);
	        		ImageLoader loader = new ImageLoader();
	        		bgmap = loader.getBitmapFromURL(imgurl);
	        		//new ImageLoader4(parentActivity).DisplayImage(imgurl, imageview, R.drawable.votenow_bg);
	        		
				}
	        }
			return result;
		}
	}
	private class GetInfoTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetInfoTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			
			if(_result != null){
				if(!_result.equals("")){
					try {
						JSONObject data = new JSONObject(_result);
						String error = data.get("status").toString();
						
						if(error.equals("1")){
							String s = data.get("content").toString();
							
							JSONObject content = new JSONObject(s);
							JsonHelper helper = new JsonHelper();
							Map<String, Object> map = helper.toMap(content);
							List<VoteItemList> votenowlist1 = new ArrayList<VoteItemList>();
							for (Object key : map.keySet()) {
								VoteItemList vlist = new VoteItemList();
								JSONObject json = (JSONObject)helper.toJSON(map.get(key));
								String costpervote = json.getString("costpervote");
								String costpercurrency = json.getString("costpercurrency");
								String votetermsconditions = json.getString("votetermsconditions");
								voteinfolist.costpercurrency = costpercurrency;
								voteinfolist.costpervote = costpervote;
								voteinfolist.votetermsconditions = votetermsconditions;
								parentActivity.selectedinfolist = voteinfolist;							
							}
							termstx.setText(voteinfolist.votetermsconditions);
						}else{
							String content = data.get("content").toString();
							ReturnMessage.showAlartDialog(parentActivity, content);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						ReturnMessage.showAlartDialog(parentActivity, "Service failed.");
					}
				}else{
					ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
				}
			}else{
				ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
			}
			((ScrollView)view.findViewById(R.id.scrollView1)).scrollTo(0, 0);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(parentActivity, "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
				APIService service = new APIService(3,0);
				String parameter = "";// reg_id=parentActivity.tokenid;
				result = service.getContentData(parameter);
	        }
			return result;
		}
	}
	private class GetConfirmTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetConfirmTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			
			if(_result != null){
				if(!_result.equals("")){
					try {
						JSONObject data = new JSONObject(_result);
						String error = data.get("status").toString();
						
						if(error.equals("1")){
							String content = data.get("content").toString();
							parentActivity.setFragment(4, parentActivity.selectednowlist.title);
						}else{
							String content = data.get("content").toString();
							ReturnMessage.showAlartDialog(parentActivity, "Vote failed.");
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						ReturnMessage.showAlartDialog(parentActivity, "Service failed.");
					}
				}else{
					ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
				}
			}else{
				ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(parentActivity, "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
				APIService service = new APIService(4,0);
				String parameter = "";// reg_id=parentActivity.tokenid;
				/*parameter = "v_event_id="+parentActivity.selectednowlist.v_event_id + "&";
				parameter = parameter + "v_item_id=" + parentActivity.selecteditemlist.v_item_id + "&";
				parameter = parameter + "reg_id=" + parentActivity.tokenid + "&";
				parameter = parameter + "pstatus=3&";
				parameter = parameter + "pemail=&";
				parameter = parameter + "pname="+parentActivity.selecteditemlist.pname+"&";
				parameter = parameter + "preference=&";
				parameter = parameter + "pamount="+parentActivity.selectedinfolist.costpervote+"&";
				parameter = parameter + "pcurrency="+parentActivity.selectedinfolist.costpercurrency+"&";
				parameter = parameter + "numberofvote=1&";
				parameter = parameter + "ostype=Android&";
				parameter = parameter + "premark="+parentActivity.selecteditemlist.premark+"&";
				*/
				parameter = "v_event_id="+parentActivity.selectednowlist.v_event_id + "&";
				parameter = parameter + "v_item_id=" + parentActivity.selecteditemlist.v_item_id + "&";
				parameter = parameter + "reg_id=" + parentActivity.tokenid + "&";
				parameter = parameter + "pstatus="+URLEncoder.encode("3")+"&";
				parameter = parameter + "pemail=&";
				parameter = parameter + "pname="+URLEncoder.encode(parentActivity.selecteditemlist.pname)+"&";
				parameter = parameter + "preference=&";
				parameter = parameter + "pamount="+URLEncoder.encode(parentActivity.selectedinfolist.costpervote)+"&";
				parameter = parameter + "pcurrency="+URLEncoder.encode(parentActivity.selectedinfolist.costpercurrency)+"&";
				parameter = parameter + "numberofvote="+URLEncoder.encode("1")+"&";
				parameter = parameter + "ostype="+URLEncoder.encode("Android")+"&";
				parameter = parameter + "premark="+URLEncoder.encode(parentActivity.selecteditemlist.premark)+"&";
				
				result = service.getContentData(parameter);
	        }
			return result;
		}
	}
	private class GetConfirmPayTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetConfirmPayTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			//MyDialog.dismiss();

			//Toast.makeText(parentActivity, "Post view OK 1", 500);
			if(_result != null){
				if(!_result.equals("")){
					try {
						JSONObject data = new JSONObject(_result);
						String error = data.get("status").toString();
						
						if(error.equals("1")){
							String content = data.get("content").toString();
							//Toast.makeText(parentActivity, "Post view OK 2", 500);
							parentActivity.setFragment(4, parentActivity.selectednowlist.title);
						}else{
							String content = data.get("content").toString();
							ReturnMessage.showAlartDialog(parentActivity, "Vote failed.");
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						ReturnMessage.showAlartDialog(parentActivity, "Service failed.");
					}
				}else{
					ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
				}
			}else{
				ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//Toast.makeText(parentActivity, "Preview OK 1", 500);
			 //MyDialog = ProgressDialog.show(parentActivity, "",
	                    //"Loading... ", false);
	            //MyDialog.setCancelable(true);
				//Toast.makeText(parentActivity, "Preview OK 2", 500);
		}

		@Override
		protected String doInBackground(Void... params) {	

			//Toast.makeText(parentActivity, "do Inbackground OK 1", 500);
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
				APIService service = new APIService(4,0);
				String parameter = "";// reg_id=parentActivity.tokenid;
				parameter = "v_event_id="+parentActivity.selectednowlist.v_event_id + "&";
				parameter = parameter + "v_item_id=" + parentActivity.selecteditemlist.v_item_id + "&";
				parameter = parameter + "reg_id=" + parentActivity.tokenid + "&";
				parameter = parameter + "pstatus="+URLEncoder.encode("1")+"&";
				parameter = parameter + "pemail="+URLEncoder.encode(mailaddress)+"&";
				parameter = parameter + "pname="+URLEncoder.encode(parentActivity.selecteditemlist.pname)+"&";
				parameter = parameter + "preference=&";
				parameter = parameter + "pamount="+URLEncoder.encode(parentActivity.selectedinfolist.costpervote)+"&";
				parameter = parameter + "pcurrency="+URLEncoder.encode(parentActivity.selectedinfolist.costpercurrency)+"&";
				parameter = parameter + "numberofvote="+URLEncoder.encode("1")+"&";
				parameter = parameter + "ostype="+URLEncoder.encode("Android")+"&";
				parameter = parameter + "premark="+URLEncoder.encode(parentActivity.selecteditemlist.premark)+"&";
				
				
				
				result = service.getContentData(parameter);
	        }
			//Toast.makeText(parentActivity, "do Inbackground OK 2", 500);
			return result;
		}
	}
}
