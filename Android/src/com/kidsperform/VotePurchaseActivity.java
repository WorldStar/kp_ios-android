package com.kidsperform;


import com.kidsperform.BillingService.RequestPurchase;
import com.kidsperform.BillingService.RestoreTransactions;
import com.kidsperform.Consts.PurchaseState;
import com.kidsperform.Consts.ResponseCode;
import com.kidsperform.lib.APIService;
import com.kidsperform.lib.CheckInternetAccess;
import com.kidsperform.utils.MyButton;
import com.kidsperform.utils.MyTextView;
import com.kidsperform.utils.ReturnMessage;
import com.kidsperform.R;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;
import com.kidsperform.R.string;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

public class VotePurchaseActivity extends Fragment implements OnClickListener,
OnItemSelectedListener {
	protected MainActivity parentActivity;
	
	View view;	

    private static final String TAG = "Dungeons";

    /**
     * Used for storing the log text.
     */
    private static final String LOG_TEXT_KEY = "DUNGEONS_LOG_TEXT";

    /**
     * The SharedPreferences key for recording whether we initialized the
     * database.  If false, then we perform a RestoreTransactions request
     * to get all the purchases for this user.
     */
    private static final String DB_INITIALIZED = "db_initialized";

    private DungeonsPurchaseObserver mDungeonsPurchaseObserver;
    private Handler mHandler;

    private BillingService mBillingService;
    private Button mBuyButton;
    private Button mEditPayloadButton;
    private Button mEditSubscriptionsButton;
    private TextView mLogTextView;
    private Spinner mSelectItemSpinner;
    private ListView mOwnedItemsTable;
    private SimpleCursorAdapter mOwnedItemsAdapter;
    private PurchaseDatabase mPurchaseDatabase;
    private Cursor mOwnedItemsCursor;
    private Set<String> mOwnedItems = new HashSet<String>();
    String mailaddress = "";
    /**
     * The developer payload that is sent with subsequent
     * purchase requests.
     */
    private String mPayloadContents = null;

    private static final int DIALOG_CANNOT_CONNECT_ID = 1;
    private static final int DIALOG_BILLING_NOT_SUPPORTED_ID = 2;
    private static final int DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID = 3;

    /**
     * Each product in the catalog can be MANAGED, UNMANAGED, or SUBSCRIPTION.  MANAGED
     * means that the product can be purchased only once per user (such as a new
     * level in a game). The purchase is remembered by Android Market and
     * can be restored if this application is uninstalled and then
     * re-installed. UNMANAGED is used for products that can be used up and
     * purchased multiple times (such as poker chips). It is up to the
     * application to keep track of UNMANAGED products for the user.
     * SUBSCRIPTION is just like MANAGED except that the user gets charged monthly
     * or yearly.
     */
    private enum Managed { MANAGED, UNMANAGED, SUBSCRIPTION }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main, null);
        parentActivity = (MainActivity)getActivity();
        
        mHandler = new Handler();
        mDungeonsPurchaseObserver = new DungeonsPurchaseObserver(mHandler);
        mBillingService = new BillingService();
        mBillingService.setContext(parentActivity);
        AccountManager am = AccountManager.get(parentActivity);
        Account[] accounts = am.getAccountsByType("com.google"); 
        
        Account mainaccount = accounts[0];
        mailaddress = mainaccount.name;
        mPurchaseDatabase = new PurchaseDatabase(parentActivity);
        setupWidgets();

        // Check if billing is supported.
        ResponseHandler.register(mDungeonsPurchaseObserver);
        if (!mBillingService.checkBillingSupported()) {
        	parentActivity.showDialog(DIALOG_CANNOT_CONNECT_ID);
        }
        
        if (!mBillingService.checkBillingSupported(Consts.ITEM_TYPE_SUBSCRIPTION)) {
        	parentActivity.showDialog(DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID);
        }
        
        
        return view;
    }
    /**
     * A {@link PurchaseObserver} is used to get callbacks when Android Market sends
     * messages to this application so that we can update the UI.
     */
    public class DungeonsPurchaseObserver extends PurchaseObserver {
        public DungeonsPurchaseObserver(Handler handler) {
            super(parentActivity, handler);
        }

        @Override
        public void onBillingSupported(boolean supported, String type) {
            if (Consts.DEBUG) {
                Log.i(TAG, "supported: " + supported);
            }
            if (type == null || type.equals(Consts.ITEM_TYPE_INAPP)) {
                if (supported) {
                    restoreDatabase();
                    mBuyButton.setEnabled(true);
                    mEditPayloadButton.setEnabled(true);
                } else {
                	parentActivity.showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
                }
            } else if (type.equals(Consts.ITEM_TYPE_SUBSCRIPTION)) {
                mCatalogAdapter.setSubscriptionsSupported(supported);
            } else {
            	parentActivity.showDialog(DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID);
            }
        }

        @Override
        public void onPurchaseStateChange(PurchaseState purchaseState, String itemId,
                int quantity, long purchaseTime, String developerPayload) {
            if (Consts.DEBUG) {
                Log.i(TAG, "onPurchaseStateChange() itemId: " + itemId + " " + purchaseState);
            }

            if (developerPayload == null) {
                //logProductActivity(itemId, purchaseState.toString());
            } else {
                //logProductActivity(itemId, purchaseState + "\n\t" + developerPayload);
            }
            
            if (purchaseState == PurchaseState.PURCHASED) {
                mOwnedItems.add(itemId);
                
                // If this is a subscription, then enable the "Edit
                // Subscriptions" button.
                for (CatalogEntry e : CATALOG) {
                    if (e.sku.equals(itemId) &&
                            e.managed.equals(Managed.SUBSCRIPTION)) {
                        mEditSubscriptionsButton.setVisibility(View.VISIBLE);
                    }
                }
            }
            mCatalogAdapter.setOwnedItems(mOwnedItems);
            mOwnedItemsCursor.requery();
        }

        @Override
        public void onRequestPurchaseResponse(RequestPurchase request,
                ResponseCode responseCode) {

        	//ReturnMessage.showAlartDialog(parentActivity, request.toString()+":"+responseCode);
            //Toast.makeText(parentActivity, "received", 500);
        	if (Consts.DEBUG) {
                Log.d(TAG, request.mProductId + ": " + responseCode);
            }
            if (responseCode == ResponseCode.RESULT_OK) {
                if (Consts.DEBUG) {
                    Log.i(TAG, "purchase was successfully sent to server");
                }
               // Toast.makeText(parentActivity, "OKOK", 500);
                //new GetConfirmTask().execute();
                logProductActivity(request.mProductId, "sending purchase request");
            } else if (responseCode == ResponseCode.RESULT_USER_CANCELED) {
                if (Consts.DEBUG) {
                    Log.i(TAG, "user canceled purchase");
                }
                //ReturnMessage.showAlartDialog(parentActivity, "dismissed purchase dialog");
                //logProductActivity(request.mProductId, "dismissed purchase dialog");
            } else {
                if (Consts.DEBUG) {
                    Log.i(TAG, "purchase failed");
                }
                //ReturnMessage.showAlartDialog(parentActivity, "request purchase returned " + responseCode);
                //logProductActivity(request.mProductId, "request purchase returned " + responseCode);
            }
        }

        @Override
        public void onRestoreTransactionsResponse(RestoreTransactions request,
                ResponseCode responseCode) {
        	//ReturnMessage.showAlartDialog(parentActivity, request.toString()+":"+responseCode);
            if (responseCode == ResponseCode.RESULT_OK) {
                if (Consts.DEBUG) {
                    Log.d(TAG, "completed RestoreTransactions request");
                }
                // Update the shared preferences so that we don't perform
                // a RestoreTransactions again.
                SharedPreferences prefs = parentActivity.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(DB_INITIALIZED, true);
                edit.commit();
            } else {
                if (Consts.DEBUG) {
                    Log.d(TAG, "RestoreTransactions error: " + responseCode);
                }
            }
        }
    }

    private static class CatalogEntry {
        public String sku;
        public int nameId;
        public Managed managed;

        public CatalogEntry(String sku, int nameId, Managed managed) {
            this.sku = sku;
            this.nameId = nameId;
            this.managed = managed;
        }
    }

    /** An array of product list entries for the products that can be purchased. */
    private static final CatalogEntry[] CATALOG = new CatalogEntry[] {
        new CatalogEntry("votetest", R.string.two_handed_sword, Managed.UNMANAGED),
        /*new CatalogEntry("potion_001", R.string.potions, Managed.UNMANAGED),
        new CatalogEntry("subscription_monthly", R.string.subscription_monthly,
                Managed.SUBSCRIPTION),
        new CatalogEntry("subscription_yearly", R.string.subscription_yearly,
                Managed.SUBSCRIPTION),
        new CatalogEntry("android.test.purchased", R.string.android_test_purchased,
                Managed.UNMANAGED),
        new CatalogEntry("android.test.canceled", R.string.android_test_canceled,
                Managed.UNMANAGED),
        new CatalogEntry("android.test.refunded", R.string.android_test_refunded,
                Managed.UNMANAGED),
        new CatalogEntry("android.test.item_unavailable", R.string.android_test_item_unavailable,
                Managed.UNMANAGED),*/
    };

    private String mItemName;
    private String mSku;
    private Managed mManagedType;
    private CatalogAdapter mCatalogAdapter;
    MyButton backbt;
    /** Called when the activity is first created. */

    /**
     * Called when this activity becomes visible.
     */
    @Override
	public void onStart() {
        super.onStart();
        ResponseHandler.register(mDungeonsPurchaseObserver);
        initializeOwnedItems();
    }

    /**
     * Called when this activity is no longer visible.
     */
    @Override
	public void onStop() {
        super.onStop();
        ResponseHandler.unregister(mDungeonsPurchaseObserver);
    }

    @Override
	public void onDestroy() {
        super.onDestroy();
        mPurchaseDatabase.close();
        mBillingService.unbind();
    }

    /**
     * Save the context of the log so simple things like rotation will not
     * result in the log being cleared.
     */
    @Override
	public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(LOG_TEXT_KEY, Html.toHtml((Spanned) mLogTextView.getText()));
    }

    /**
     * Restore the contents of the log if it has previously been saved.
     */
    /*@Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mLogTextView.setText(Html.fromHtml(savedInstanceState.getString(LOG_TEXT_KEY)));
        }
    }*/



    

    /**
     * Sets up the UI.
     */
    private void setupWidgets() {
        mLogTextView = (TextView) view.findViewById(R.id.log);

        mBuyButton = (Button) view.findViewById(R.id.buy_button);
        mBuyButton.setEnabled(false);
        mBuyButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//logProductActivity("asdasdasd", "sending purchase request");
				if (Consts.DEBUG) {
	                Log.d(TAG, "buying: " + mItemName + " sku: " + mSku);
	            }

	            if (mManagedType != Managed.SUBSCRIPTION &&
	                    !mBillingService.requestPurchase(mSku, Consts.ITEM_TYPE_INAPP, mPayloadContents)) {
	            	parentActivity.showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
	            } else if (!mBillingService.requestPurchase(mSku, Consts.ITEM_TYPE_SUBSCRIPTION, mPayloadContents)) {
	                // Note: mManagedType == Managed.SUBSCRIPTION
	            	parentActivity.showDialog(DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID);
	            }
	            
			}
		});

        //backbt = (MyButton)view.findViewById(R.id.close_button);
        backbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				parentActivity.setFragment(4, parentActivity.selectednowlist.title);
			}
		});
        mEditPayloadButton = (Button) view.findViewById(R.id.payload_edit_button);
        mEditPayloadButton.setEnabled(false);
        mEditPayloadButton.setOnClickListener(parentActivity);

        mEditSubscriptionsButton = (Button) view.findViewById(R.id.subscriptions_edit_button);
        mEditSubscriptionsButton.setVisibility(View.INVISIBLE);
        mEditSubscriptionsButton.setOnClickListener(parentActivity);

        mSelectItemSpinner = (Spinner) view.findViewById(R.id.item_choices);
        mCatalogAdapter = new CatalogAdapter(parentActivity, CATALOG);
        mSelectItemSpinner.setAdapter(mCatalogAdapter);
        mSelectItemSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				mItemName = getString(CATALOG[position].nameId);
		        mSku = CATALOG[position].sku;
		        mManagedType = CATALOG[position].managed;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        	
        });

        mOwnedItemsCursor = mPurchaseDatabase.queryAllPurchasedItems();
        parentActivity.startManagingCursor(mOwnedItemsCursor);
        String[] from = new String[] { PurchaseDatabase.PURCHASED_PRODUCT_ID_COL,
                PurchaseDatabase.PURCHASED_QUANTITY_COL
        };
        int[] to = new int[] { R.id.item_name, R.id.item_quantity };
        mOwnedItemsAdapter = new SimpleCursorAdapter(parentActivity, R.layout.item_row,
                mOwnedItemsCursor, from, to);
        mOwnedItemsTable = (ListView) view.findViewById(R.id.owned_items);
        mOwnedItemsTable.setAdapter(mOwnedItemsAdapter);
    }

    private void prependLogEntry(CharSequence cs) {
        SpannableStringBuilder contents = new SpannableStringBuilder(cs);
        contents.append('\n');
        contents.append(mLogTextView.getText());
        mLogTextView.setText(contents);
    }

    private void logProductActivity(String product, String activity) {
        /*SpannableStringBuilder contents = new SpannableStringBuilder();
        contents.append(Html.fromHtml("<b>" + product + "</b>: "));
        contents.append(activity);
        prependLogEntry(contents);
        */
    	new GetConfirmTask().execute();
    }

    /**
     * If the database has not been initialized, we send a
     * RESTORE_TRANSACTIONS request to Android Market to get the list of purchased items
     * for this user. This happens if the application has just been installed
     * or the user wiped data. We do not want to do this on every startup, rather, we want to do
     * only when the database needs to be initialized.
     */
    private void restoreDatabase() {
        SharedPreferences prefs = parentActivity.getPreferences(parentActivity.MODE_PRIVATE);
        boolean initialized = prefs.getBoolean(DB_INITIALIZED, false);
        if (!initialized) {
            mBillingService.restoreTransactions();
            Toast.makeText(parentActivity, R.string.restoring_transactions, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Creates a background thread that reads the database and initializes the
     * set of owned items.
     */
    private void initializeOwnedItems() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                doInitializeOwnedItems();
            }
        }).start();
    }

    /**
     * Reads the set of purchased items from the database in a background thread
     * and then adds those items to the set of owned items in the main UI
     * thread.
     */
    private void doInitializeOwnedItems() {
        Cursor cursor = mPurchaseDatabase.queryAllPurchasedItems();
        if (cursor == null) {
            return;
        }

        final Set<String> ownedItems = new HashSet<String>();
        try {
            int productIdCol = cursor.getColumnIndexOrThrow(
                    PurchaseDatabase.PURCHASED_PRODUCT_ID_COL);
            while (cursor.moveToNext()) {
                String productId = cursor.getString(productIdCol);
                ownedItems.add(productId);
            }
        } finally {
            cursor.close();
        }

        // We will add the set of owned items in a new Runnable that runs on
        // the UI thread so that we don't need to synchronize access to
        // mOwnedItems.
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mOwnedItems.addAll(ownedItems);
                mCatalogAdapter.setOwnedItems(mOwnedItems);
            }
        });
    }

    /**
     * Called when a button is pressed.
     */
    @Override
    public void onClick(View v) {
        //if (v == mBuyButton) {
           /* if (Consts.DEBUG) {
                Log.d(TAG, "buying: " + mItemName + " sku: " + mSku);
            }

            if (mManagedType != Managed.SUBSCRIPTION &&
                    !mBillingService.requestPurchase(mSku, Consts.ITEM_TYPE_INAPP, mPayloadContents)) {
            	parentActivity.showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
            } else if (!mBillingService.requestPurchase(mSku, Consts.ITEM_TYPE_SUBSCRIPTION, mPayloadContents)) {
                // Note: mManagedType == Managed.SUBSCRIPTION
            	parentActivity.showDialog(DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID);
            }*/
        	
        /*} else if (v == mEditPayloadButton) {
            showPayloadEditDialog();
        } else if (v == mEditSubscriptionsButton) {
            editSubscriptions();
        }else if(v == backbt){
        	
        }*/
    }

    /** List subscriptions for this package in Google Play
     *
     * This allows users to unsubscribe from this apps subscriptions.
     *
     * Subscriptions are listed on the Google Play app detail page, so this
     * should only be called if subscriptions are known to be present.
     */
    private void editSubscriptions() {
        // Get current package name
        String packageName = parentActivity.getPackageName();
        // Open app detail in Google Play
        Intent i = new Intent(Intent.ACTION_VIEW,
                             Uri.parse("market://details?id=" + packageName));
        startActivity(i);
    }

    /**
     * Displays the dialog used to edit the payload dialog.
     */
    private void showPayloadEditDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(parentActivity);
        final View view = View.inflate(parentActivity, R.layout.edit_payload, null);
        final TextView payloadText = (TextView) view.findViewById(R.id.payload_text);
        if (mPayloadContents != null) {
            payloadText.setText(mPayloadContents);
        }

        dialog.setView(view);
        dialog.setPositiveButton(
                R.string.edit_payload_accept,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPayloadContents = payloadText.getText().toString();
                    }
                });
        dialog.setNegativeButton(
                R.string.edit_payload_clear,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialog != null) {
                            mPayloadContents = null;
                            dialog.cancel();
                        }
                    }
                });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (dialog != null) {
                    dialog.cancel();
                }
            }
        });
        dialog.show();
    }

    /**
     * Called when an item in the spinner is selected.
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mItemName = getString(CATALOG[position].nameId);
        mSku = CATALOG[position].sku;
        mManagedType = CATALOG[position].managed;
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    /**
     * An adapter used for displaying a catalog of products.  If a product is
     * managed by Android Market and already purchased, then it will be "grayed-out" in
     * the list and not selectable.
     */
    private static class CatalogAdapter extends ArrayAdapter<String> {
        private CatalogEntry[] mCatalog;
        private Set<String> mOwnedItems = new HashSet<String>();
        private boolean mIsSubscriptionsSupported = false;

        public CatalogAdapter(Context context, CatalogEntry[] catalog) {
            super(context, android.R.layout.simple_spinner_item);
            mCatalog = catalog;
            for (CatalogEntry element : catalog) {
                add(context.getString(element.nameId));
            }
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }

        public void setOwnedItems(Set<String> ownedItems) {
            mOwnedItems = ownedItems;
            notifyDataSetChanged();
        }

        public void setSubscriptionsSupported(boolean supported) {
            mIsSubscriptionsSupported = supported;
        }

        @Override
        public boolean areAllItemsEnabled() {
            // Return false to have the adapter call isEnabled()
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            // If the item at the given list position is not purchasable,
            // then prevent the list item from being selected.
            CatalogEntry entry = mCatalog[position];
            if (entry.managed == Managed.MANAGED && mOwnedItems.contains(entry.sku)) {
                return false;
            }
            if (entry.managed == Managed.SUBSCRIPTION && !mIsSubscriptionsSupported) {
                return false;
            }
            return true;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            // If the item at the given list position is not purchasable, then
            // "gray out" the list item.
            View view = super.getDropDownView(position, convertView, parent);
            view.setEnabled(isEnabled(position));
            return view;
        }
    }
	private class GetConfirmTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetConfirmTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			//MyDialog.dismiss();

			//Toast.makeText(parentActivity, "Post view OK 1", 500);
			if(_result != null){
				if(!_result.equals("")){
					try {
						JSONObject data = new JSONObject(_result);
						String error = data.get("status").toString();
						
						if(error.equals("1")){
							String content = data.get("content").toString();
							//Toast.makeText(parentActivity, "Post view OK 2", 500);
							parentActivity.setFragment(4, parentActivity.selectednowlist.title);
						}else{
							String content = data.get("content").toString();
							ReturnMessage.showAlartDialog(parentActivity, "Vote failed.");
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						ReturnMessage.showAlartDialog(parentActivity, "Service failed.");
					}
				}else{
					ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
				}
			}else{
				ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//Toast.makeText(parentActivity, "Preview OK 1", 500);
			 //MyDialog = ProgressDialog.show(parentActivity, "",
	                    //"Loading... ", false);
	            //MyDialog.setCancelable(true);
				//Toast.makeText(parentActivity, "Preview OK 2", 500);
		}

		@Override
		protected String doInBackground(Void... params) {	

			//Toast.makeText(parentActivity, "do Inbackground OK 1", 500);
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
				APIService service = new APIService(4,0);
				String parameter = "";// reg_id=parentActivity.tokenid;
				parameter = "v_event_id="+parentActivity.selectednowlist.v_event_id + "&";
				parameter = parameter + "v_item_id=" + parentActivity.selecteditemlist.v_item_id + "&";
				parameter = parameter + "reg_id=" + parentActivity.tokenid + "&";
				parameter = parameter + "pstatus="+URLEncoder.encode("1")+"&";
				parameter = parameter + "pemail="+URLEncoder.encode(mailaddress)+"&";
				parameter = parameter + "pname="+URLEncoder.encode(parentActivity.selecteditemlist.pname)+"&";
				parameter = parameter + "preference=&";
				parameter = parameter + "pamount="+URLEncoder.encode(parentActivity.selectedinfolist.costpervote)+"&";
				parameter = parameter + "pcurrency="+URLEncoder.encode(parentActivity.selectedinfolist.costpercurrency)+"&";
				parameter = parameter + "numberofvote="+URLEncoder.encode("1")+"&";
				parameter = parameter + "ostype="+URLEncoder.encode("Android")+"&";
				parameter = parameter + "premark="+URLEncoder.encode(parentActivity.selecteditemlist.premark)+"&";
				
				result = service.getContentData(parameter);
	        }
			//Toast.makeText(parentActivity, "do Inbackground OK 2", 500);
			return result;
		}
	}
}
