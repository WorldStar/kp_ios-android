package com.kidsperform;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.google.android.gcm.GCMRegistrar;
import com.kidsperform.data.MenuList;
import com.kidsperform.data.VoteEList;
import com.kidsperform.data.VoteInfoList;
import com.kidsperform.data.VoteItemList;
import com.kidsperform.utils.MyButton;
import com.kidsperform.utils.MyTextView;
import com.kidsperform.R.array;
import com.kidsperform.R.drawable;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;
import com.kidsperform.R.string;
import com.kidsperform.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements OnClickListener, OnItemSelectedListener{

	MainLayout mLayout;
	private ListView lvMenu;
	private String[] lvMenuItems;
	private int[] lvMenuIcons = {R.drawable.menu1,R.drawable.menu2,R.drawable.menu3,R.drawable.menu6,R.drawable.menu7,R.drawable.menu8};//,R.drawable.menu9
	Button btMenu;
	LinearLayout back_layout;
	MyTextView tvTitle;
	List<MenuList> menulist = new ArrayList<MenuList>();
	public int select_id = 10;
	public static AdapterView<?> oldparent;
	public static View oldview;
	public int votedepth = 0;	
	public static VoteEList selectednowlist = null;
	public static VoteItemList selecteditemlist = null;
	public static VoteInfoList selectedinfolist = null;
	Controller aController;
	public static String tokenid = "";
	public static int total = 0;
	public static float depth = 0;
	public static int width = 0;
	public static int height = 0;
	public static MainActivity mParent = null;

    private static final int DIALOG_CANNOT_CONNECT_ID = 1;
    private static final int DIALOG_BILLING_NOT_SUPPORTED_ID = 2;
    private static final int DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID = 3;
    private static final String TAG = "KP";
	//public static boolean startflg = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mLayout = (MainLayout) this.getLayoutInflater().inflate(
				R.layout.activity_main, null);
		setContentView(mLayout);
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		depth = metrics.density;
		
		Display display = getWindowManager().getDefaultDisplay(); 
		width = display.getWidth();  // deprecated
		height = display.getHeight(); 
		
		lvMenuItems = getResources().getStringArray(R.array.menu_items);

		lvMenu = (ListView) findViewById(R.id.menu_listview);
		for(int i = 0; i < lvMenuItems.length; i++){
			MenuList m = new MenuList();
			m.img = lvMenuIcons[i];
			m.title = lvMenuItems[i];
			menulist.add(m);
		}
		MenuAdapter menuadapter = new MenuAdapter(this, R.layout.menulist, menulist);
		lvMenu.setAdapter(menuadapter);
		lvMenu.setSmoothScrollbarEnabled(true);
		lvMenu.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				oldparent = parent;
				oldview = view;
				onMenuItemClick(parent, view, position, id);
			}

		});
		LinearLayout menubt_layout = (LinearLayout)findViewById(R.id.menubt_layout);
		btMenu = (Button) findViewById(R.id.button_menu);
		menubt_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Show/hide the menu
				toggleMenu(v);
			}
		});
		back_layout = (LinearLayout)findViewById(R.id.back_layout);
		back_layout.setVisibility(View.GONE);
		back_layout.setOnClickListener(this);
		MyButton backbt = (MyButton)findViewById(R.id.myButton1);
		backbt.setOnClickListener(this);
		tvTitle = (MyTextView) findViewById(R.id.activity_main_content_title);
		////**************/////
		aController = (Controller) getApplicationContext();
      	// Check if Internet present
 		if (!aController.isConnectingToInternet()) {
 			
 			// Internet Connection is not present
 			aController.showAlertDialog(this,
 					"Internet Connection Error",
 					"Internet failed,Please Try Again", false);
 			// stop executing code by return
 		}
 		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);

		// Make sure the manifest permissions was properly set 
		GCMRegistrar.checkManifest(this);
		// Register custom Broadcast receiver to show messages on activity
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				Config.DISPLAY_MESSAGE_ACTION));
		
		// Get GCM registration id
		String regId = GCMRegistrar.getRegistrationId(this);
		tokenid = regId;
		// Check if regid already presents
		if (regId.equals("")) {
			
			// Register with GCM			
			GCMRegistrar.register(this, Config.GOOGLE_SENDER_ID);
			regId = GCMRegistrar.getRegistrationId(this);
			tokenid = regId;
		}
		//////end//////
		FragmentManager fm = MainActivity.this.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		VoteActivity fragment = new VoteActivity();
		ft.add(R.id.activity_main_content_fragment, fragment);
		ft.commit();
		
		mParent = this;

	}


	public void toggleMenu(View v) {
		mLayout.toggleMenu();
	}

	private void onMenuItemClick(AdapterView<?> parent, View view,
			int position, long id) {
		String regId = GCMRegistrar.getRegistrationId(this);
		tokenid = regId;
		
		String selectedItem = lvMenuItems[position];
		String currentItem = tvTitle.getText().toString();
		select_id = position;
		MenuAdapter menuadapter = new MenuAdapter(this, R.layout.menulist, menulist);
		lvMenu.setAdapter(menuadapter);
		lvMenu.setSmoothScrollbarEnabled(true);
		lvMenu.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				onMenuItemClick(parent, view, position, id);
			}

		});
		if (selectedItem.compareTo(currentItem) == 0) {
			mLayout.toggleMenu();
			return;
		}

		FragmentManager fm = MainActivity.this.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment fragment = null;

		if (selectedItem.compareTo("About Us") == 0) {
			fragment = new AboutActivity();
		} else if (selectedItem.compareTo("Vote") == 0) {
			//if(startflg){
				fragment = new VoteProcessActivity();
			//}else{
				//fragment = new VoteActivity();
			//}
		} else if (selectedItem.compareTo("Classes") == 0) {
			fragment = new CoursesActivity();
		} else if (selectedItem.compareTo("Trainers") == 0) {
			fragment = new TrainersActivity();
		} else if (selectedItem.compareTo("Testimonial") == 0) {
			fragment = new TestActivity();
		} else if (selectedItem.compareTo("Events") == 0) {
			fragment = new EventActivity();
		} else if (selectedItem.compareTo("Contact Us") == 0) {
			fragment = new ContactActivity();
		} else if (selectedItem.compareTo("Terms & Conditions") == 0) {
			fragment = new TermsActivity();
		} else if (selectedItem.compareTo("Policy") == 0) {
			fragment = new PolicyActivity();
		}else{
			fragment = new VoteActivity();
		}

		if (fragment != null) {
			ft.replace(R.id.activity_main_content_fragment, fragment);
			ft.commit();
			tvTitle.setText(selectedItem+"     ");
			back_layout.setVisibility(View.GONE);
			btMenu.setVisibility(View.VISIBLE);
		}
		mLayout.toggleMenu();
		//startflg = true;
	}
	public void setFragment(int f_id, String title){
		String regId = GCMRegistrar.getRegistrationId(this);
		tokenid = regId;
		//sub screen enter
		FragmentManager fm = MainActivity.this.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment fragment = null;
		switch(f_id){
		case 1:
			fragment = new VoteItemActivity();
			votedepth = 1;
			break;
		case 2:
			fragment = new VoteDetailsActivity();
			votedepth = 2;
			break;
		case 3:
			fragment = new VotePurchaseActivity();
			votedepth = 3;
			break;
		case 4:
			fragment = new VoteConfirmActivity();
			votedepth = 4;
			break;
		}
		if (fragment != null) {
			ft.replace(R.id.activity_main_content_fragment, fragment);
			ft.commit();
			int f = (int)((float)20 * depth / 2.0f);
			if(title.length() > 20){
				title = title.substring(0, 20) + "...";
			}
			tvTitle.setText(title+"     ");
			back_layout.setVisibility(View.VISIBLE);
			btMenu.setVisibility(View.GONE);
			if(votedepth == 4){
				back_layout.setVisibility(View.GONE);
			}
		}
	}
	public void setFragment1(int f_id, String title){
		String regId = GCMRegistrar.getRegistrationId(this);
		tokenid = regId;
		// main screen enter
		FragmentManager fm = MainActivity.this.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment fragment = null;
		switch(f_id){
		case 1:
			fragment = new VoteProcessActivity();
			select_id = 1;
			MenuAdapter menuadapter = new MenuAdapter(this, R.layout.menulist, menulist);
			lvMenu.setAdapter(menuadapter);
			lvMenu.setSmoothScrollbarEnabled(true);
			lvMenu.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					onMenuItemClick(parent, view, position, id);
				}

			});
			votedepth = 0;
			break;
		}
		if (fragment != null) {
			ft.replace(R.id.activity_main_content_fragment, fragment);
			ft.commit();
			tvTitle.setText(title+"     ");
			back_layout.setVisibility(View.GONE);
			btMenu.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.back_layout){
			switch(votedepth){
			case 1:
				setFragment1(1, "Vote");
				break;
			case 2:
				setFragment(1, selectednowlist.title);
				break;
			case 3:
				setFragment(2, selectednowlist.title);
				break;
			}
		}else if(v.getId() == R.id.myButton1){
			switch(votedepth){
			case 1:
				setFragment1(1, "Vote");
				break;
			case 2:
				setFragment(1, selectednowlist.title);
				break;
			case 3:
				setFragment(2, selectednowlist.title);
				break;
			}
		}
	}
	@Override
	public void onBackPressed() {
		if (mLayout.isMenuShown()) {
			mLayout.toggleMenu();
		} else {
			super.onBackPressed();
		}
	}
	public class MenuAdapter extends ArrayAdapter<MenuList>
	{


		private List<MenuList>items;
		public MenuAdapter(Context context, int textViewResourceId,
				List<MenuList> objects) {
			
			super(context, textViewResourceId, objects);
			this.items=objects;
			// TODO Auto-generated constructor stub
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final MenuList item=items.get(position);
			if(v==null)
			{
				v = vi.inflate(R.layout.menulist, null);
			}
			final int po = position;
			if(po == select_id){
				v.setBackgroundResource(R.drawable.menu_select);
			}
			MyTextView title = (MyTextView)v.findViewById(R.id.textView1);
			title.setText(item.title);
			ImageView img = (ImageView)v.findViewById(R.id.imageView1);
			img.setBackgroundDrawable(getResources().getDrawable(item.img));
			return v;
		}
		
	}
	// Create a broadcast receiver to get message and show on screen 
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);
			
			// Waking up mobile if it is sleeping
			aController.acquireWakeLock(getApplicationContext());
					
			
			//Toast.makeText(getApplicationContext(), "Got Message: " + newMessage, Toast.LENGTH_LONG).show();
			
			// Releasing wake lock
			aController.releaseWakeLock();
		}
	};
	@Override
	protected void onDestroy() {
		try {
			// Unregister Broadcast Receiver
			unregisterReceiver(mHandleMessageReceiver);
			
			//Clear internal resources.
			GCMRegistrar.onDestroy(this);
			
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_CANNOT_CONNECT_ID:
            return createDialog(R.string.cannot_connect_title,
                    R.string.cannot_connect_message);
        case DIALOG_BILLING_NOT_SUPPORTED_ID:
            return createDialog(R.string.billing_not_supported_title,
                    R.string.billing_not_supported_message);
            case DIALOG_SUBSCRIPTIONS_NOT_SUPPORTED_ID:
                return createDialog(R.string.subscriptions_not_supported_title,
                        R.string.subscriptions_not_supported_message);
        default:
            return null;
        }
    }

    private Dialog createDialog(int titleId, int messageId) {
        String helpUrl = replaceLanguageAndRegion(getString(R.string.help_url));
        if (Consts.DEBUG) {
            Log.i(TAG, helpUrl);
        }
        final Uri helpUri = Uri.parse(helpUrl);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleId)
            .setIcon(android.R.drawable.stat_sys_warning)
            .setMessage(messageId)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok, null)
            .setNegativeButton(R.string.learn_more, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, helpUri);
                    startActivity(intent);
                }
            });
        return builder.create();
    }
	/**
     * Replaces the language and/or country of the device into the given string.
     * The pattern "%lang%" will be replaced by the device's language code and
     * the pattern "%region%" will be replaced with the device's country code.
     *
     * @param str the string to replace the language/country within
     * @return a string containing the local language and region codes
     */
    private String replaceLanguageAndRegion(String str) {
        // Substitute language and or region if present in string
        if (str.contains("%lang%") || str.contains("%region%")) {
            Locale locale = Locale.getDefault();
            str = str.replace("%lang%", locale.getLanguage().toLowerCase());
            str = str.replace("%region%", locale.getCountry().toLowerCase());
        }
        return str;
    }


	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
