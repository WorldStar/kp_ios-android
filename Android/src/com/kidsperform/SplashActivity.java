package com.kidsperform;

import com.kidsperform.R;
import com.kidsperform.R.layout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;


public class SplashActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		Thread splashTimer = new Thread(){
			public void run(){
				try{
					int splashTimer = 0;
					while (splashTimer < 3000){
						sleep (100);
						splashTimer += 100;
					}
						
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
					
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					finish();
				}
			}
			
		};
		
		splashTimer.start();
	}
	
}
