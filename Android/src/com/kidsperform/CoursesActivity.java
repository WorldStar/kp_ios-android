package com.kidsperform;


import com.kidsperform.lib.APIService;
import com.kidsperform.R;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class CoursesActivity extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.course, null);
        WebView mWebView = null;
        mWebView = (WebView)view.findViewById(R.id.webView1);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(APIService.IMG_URL+APIService.HTML_URL+"classes.html");
        return view;
    }
}
