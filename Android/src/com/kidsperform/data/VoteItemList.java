package com.kidsperform.data;

import android.graphics.Bitmap;

public class VoteItemList {
	public int v_event_id = 0;
	public int v_item_id = 0;
	public String name = "";
	public String desc = "";
	public String imgurl = "";
	public String status = "";
	public int numbersofvote = 0;
	public float percent = 0;
	public String pvalue = "0";
	public String pname = "";
	public String premark = "";
	public Bitmap bitmap = null;
}
