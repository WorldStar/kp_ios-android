package com.kidsperform;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.kidsperform.MainActivity.MenuAdapter;
import com.kidsperform.VoteProcessActivity.VoteAdapter;
import com.kidsperform.data.MenuList;
import com.kidsperform.data.VoteEList;
import com.kidsperform.data.VoteItemList;
import com.kidsperform.lib.APIService;
import com.kidsperform.lib.CheckInternetAccess;
import com.kidsperform.lib.ImageLoader;
import com.kidsperform.lib.ImageLoader2;
import com.kidsperform.lib.ImageLoader3;
import com.kidsperform.lib.ImageLoader4;
import com.kidsperform.lib.JsonHelper;
import com.kidsperform.utils.MyTextView;
import com.kidsperform.utils.ReturnMessage;
import com.kidsperform.R;
import com.kidsperform.R.color;
import com.kidsperform.R.drawable;
import com.kidsperform.R.id;
import com.kidsperform.R.layout;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class VoteItemActivity extends Fragment {
	protected MainActivity parentActivity;
	List<VoteItemList> voteitemlist = new ArrayList<VoteItemList>();
	private ListView lvvote1;
	View view;
	private ImageLoader2 imageLoader;
	VoteEList velist;
	ImageView imgview;
	//LinearLayout vitem_layout ;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.vote_item, null);
        lvvote1 = (ListView)view.findViewById(R.id.listView1);
        parentActivity = (MainActivity)getActivity();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setImageLoader(new ImageLoader2(parentActivity, ImageLoader2.LIST_IMAGE));
        velist = parentActivity.selectednowlist;
        //vitem_layout = (LinearLayout)view.findViewById(R.id.vitem_layout);
        imgview = (ImageView)view.findViewById(R.id.imageView1);
        parentActivity.total = 0;
        

        
        return view;
    }
	public void setImageLoader(ImageLoader2 imageLoader2) {
		this.imageLoader = imageLoader2;
	}

	public ImageLoader2 getImageLoader() {
		return imageLoader;
	}
	private void onVoteItemClick(AdapterView<?> parent, View view,
			int position, long id, Bitmap bitmap) {
		clearAllResources();
		parentActivity.selecteditemlist = voteitemlist.get(position);
		parentActivity.selecteditemlist.bitmap = bitmap;
		lvvote1.destroyDrawingCache();
		//lvvote1.removeAllViews();
		parentActivity.setFragment(2, parentActivity.selectednowlist.title);
	}
	public class VoteItemAdapter extends ArrayAdapter<VoteItemList>
	{


		private List<VoteItemList>items;
		ImageLoader2 imgLoader;
		Context con;
		public VoteItemAdapter(Context context, int textViewResourceId,
				List<VoteItemList> objects, ImageLoader2 imgLoader) {
			
			super(context, textViewResourceId, objects);
			this.items=objects;
			this.imgLoader = imgLoader;
			this.con = context;
			// TODO Auto-generated constructor stub
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v=convertView;
			LayoutInflater vi = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final VoteItemList item=items.get(position);
			if(v==null)
			{
				v = vi.inflate(R.layout.voteitemlist, null);
			}
			final int po = position;
			MyTextView title = (MyTextView)v.findViewById(R.id.name);
			String s = item.name;
			int f = (int)((float)25 * parentActivity.depth / 2.0f);
			if(parentActivity.depth > 2.0){
				f = (int)((float)25 * parentActivity.depth / 3.0f);
			}
			if(item.name.length() > f){
				s = item.name.substring(0, f) + "...";
			}
			title.setText(s);
			//MyTextView topvname = (MyTextView)v.findViewById(R.id.topname);
			LinearLayout iconlayout = (LinearLayout)v.findViewById(R.id.icon_layout);
			MyTextView icontx = (MyTextView)v.findViewById(R.id.icon_tx);
			if(item.status.equals("0")){
				iconlayout.setBackgroundDrawable(view.getContext().getResources().getDrawable(R.drawable.voteitem_icon_2));
				icontx.setTextColor(view.getContext().getResources().getColor(R.color.white));
			}else{
				iconlayout.setBackgroundDrawable(view.getContext().getResources().getDrawable(R.drawable.voteitem_icon_3));
				icontx.setTextColor(view.getContext().getResources().getColor(R.color.gray));
				
			}
			// caculate percent
			float percent = 0;
			/**
			 * calculating here
			 */
			if(parentActivity.total != 0){
				percent = (float)(100 * (float)item.numbersofvote) / (float)parentActivity.total;
			}
			String pvalue = "" + (int)percent;
			item.percent = percent;
			item.pvalue = pvalue;
			//130-4-4
			LinearLayout line_layout = (LinearLayout)v.findViewById(R.id.line_layout);
			int val = (int)((float)(120) * percent / 100);
			int px = APIService.convertDpToPixel(val, parentActivity);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(px, 8);
			line_layout.setLayoutParams(params);
			MyTextView p = (MyTextView)v.findViewById(R.id.percent);
			p.setText(pvalue+"%");
			if(!item.status.equals("0")){
				line_layout.setBackgroundColor(con.getResources().getColor(R.color.gray));
			}
			//topvname.setText(item.topvname);
			final ImageView usericon = (ImageView)v.findViewById(R.id.usericon);
			final String imgurl = APIService.IMG_URL+ APIService.PHOTO_URL + item.imgurl;
			if(imgurl != null){
				usericon.setTag(imgurl);
				imgLoader.DisplayImage2(
						imgurl, 
						parentActivity, 
						usericon,
						null, R.drawable.icon_user);
				 
				//new ImageLoader3(parentActivity).DisplayImage(imgurl, usericon, R.drawable.icon_user);
			}
			v.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(voteitemlist.get(po).status.equals("0")){
						Bitmap bitmap = ((BitmapDrawable)usericon.getDrawable()).getBitmap();
						
						onVoteItemClick(null, view, po, 0, bitmap);
					}
				}
			});
			return v;
		}
		
	}
	public void clearAllResources() {

	    // Set related variables null

	}
    @Override
	public void onStart() {
        super.onStart();

        System.gc();
        new GetBGTask().execute();
    }
    @Override
	public void onDestroy() {
        super.onDestroy();

        imgview = null;
        //getImageLoader().clearCache();
        System.gc();
    }
	private class GetBGTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
		Bitmap bgmap = null;
       public GetBGTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(bgmap != null){
				imgview.setImageBitmap(bgmap);
				//BitmapDrawable background = new BitmapDrawable(bgmap);
				//imgview.setImageDrawable(background);
				//bgmap.recycle();
				if(bgmap != null){
					bgmap = null;
				}
			}
			new GetEventTask().execute();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(parentActivity, "",
	                    "Loading background image... ", false);
	            MyDialog.setCancelable(false);
	            /*CheckInternetAccess check = new CheckInternetAccess(parentActivity);
		        if (check.checkNetwork()) {
		        	if(velist.burl != null){
		        		String imgurl = APIService.IMG_URL+APIService.BG_URL+velist.burl;
		        		imgview.setTag(imgurl);
		        		getImageLoader().DisplayImage2(
								imgurl, 
								parentActivity, 
								imgview,
								null, R.drawable.votenow_bg);
						
		        		//new ImageLoader4(parentActivity).DisplayImage(imgurl, imgview, R.drawable.votenow_bg);
		        		
					}
		        }*/
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
	        	if(velist.burl != null){
	        		String imgurl = APIService.IMG_URL+APIService.BG_URL+velist.burl;
	        		imgview.setTag(imgurl);
	        		ImageLoader loader = new ImageLoader();
	        		bgmap = loader.getBitmapFromURL(imgurl);
	        		
				}
	        }
			return result;
		}
	}
	private class GetEventTask extends
	AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetEventTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			
			if(_result != null){
				voteitemlist.clear();
				if(!_result.equals("")){
					try {
						JSONObject data = new JSONObject(_result);
						String error = data.get("status").toString();
						
						if(error.equals("1")){
							String s = data.get("content").toString();
							
							JSONObject content = new JSONObject(s);
							JsonHelper helper = new JsonHelper();
							LinkedHashMap<String, Object> map = helper.toMap(content);
							Set<String> keys = map.keySet();

					        List<String> keyColList = new ArrayList<String>();
					        keyColList.addAll(keys);
					        IgnoreCaseComparator icc = new IgnoreCaseComparator();
					        java.util.Collections.sort(keyColList,icc);
					       
							List<VoteEList> votenowlist1 = new ArrayList<VoteEList>();
							for (String keyIndex:keyColList) {	
								VoteItemList vlist = new VoteItemList();
								JSONObject json = (JSONObject)helper.toJSON(map.get(keyIndex));
								String voteEventid = json.getString("voteEventid");
								String voteItemid = json.getString("voteItemid");
								String createddate = json.getString("createddate");
								String name = json.getString("name");
								String description = json.getString("description");
								String imageURL = json.getString("imageURL");
								String status = json.getString("status");
								String NumbersofVote = json.getString("NumbersofVote");
								
								
								
								vlist.name = name;
								vlist.desc = description;
								vlist.imgurl = imageURL;
								int num = 0;
								if(NumbersofVote != null && !NumbersofVote.equals("")){
									num = Integer.parseInt(NumbersofVote);
								}
								parentActivity.total +=num;
								
								vlist.numbersofvote = num;
								vlist.status = status;
								int vid = 0;
								if(voteEventid != null && !voteEventid.equals("")){
									vid = Integer.parseInt(voteEventid);
								}
								vlist.v_event_id = vid;
								int viid = 0;
								if(voteItemid != null && !voteItemid.equals("")){
									viid = Integer.parseInt(voteItemid);
								}
								vlist.v_item_id = viid;
								
								voteitemlist.add(vlist);								
							}
							/*for(int i = votenowlist1.size()-1 ; i >= 0; i--){
								VoteItemList vlist = votenowlist1.get(i);
								voteitemlist.add(vlist);
							}*/
							VoteItemAdapter menuadapter = new VoteItemAdapter(view.getContext(), R.layout.voteitemlist, voteitemlist, getImageLoader());
					        lvvote1.setAdapter(menuadapter);
					        lvvote1.setSmoothScrollbarEnabled(true);
					        /*lvvote1.setOnItemClickListener(new OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
									if(voteitemlist.get(position).status.equals("0")){
										onVoteItemClick(parent, view, position, id);
									}				
								}

							});*/
						}else{
							String content = data.get("content").toString();
							ReturnMessage.showAlartDialog(parentActivity, content);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						ReturnMessage.showAlartDialog(parentActivity, "Service failed.");
					}
				}else{
					ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
				}
			}else{
				ReturnMessage.showAlartDialog(parentActivity, "Internet connection failed. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
     		 MyDialog = ProgressDialog.show(parentActivity, "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(parentActivity);
	        if (check.checkNetwork()) {
				APIService service = new APIService(2,0);
				String parameter = "v_event_id=" + velist.v_event_id+"&reg_id="+parentActivity.tokenid;
				result = service.getContentData(parameter);
	        }
			return result;
		}
	}
	class IgnoreCaseComparator implements Comparator<String> {
	  public int compare(String strA, String strB) {
		  int a = Integer.parseInt(strA);
		  int b = Integer.parseInt(strB);
		  int k = 0;
		  
	    return a-b;//strA.compareToIgnoreCase(strB);
	  }
	}
}
