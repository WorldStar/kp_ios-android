//
//  AsyncImageView.m
//  MyKidsOnFilm
//
//  Created by osone on 9/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AsyncImageView.h"


@implementation AsyncImageView

+ (CGRect) GetInnerRect:(CGSize)contentsize :(CGSize)outsize {
    float rate1 = contentsize.height / contentsize.width;
    float rate2 = outsize.height / outsize.width;
    CGSize resultsize;
    
    if (rate1 > rate2) {
        resultsize.height = outsize.height;
        resultsize.width = resultsize.height / rate1;
    }
    else {
        resultsize.width = outsize.width;
        resultsize.height = resultsize.width * rate1;
    }
    
    return CGRectMake((outsize.width - resultsize.width)/2, (outsize.height - resultsize.height) / 2, resultsize.width, resultsize.height);
}

- (void)loadImageFromURL:(NSURL*)url {
    if (connection!=nil) { [connection release]; }
    if (data!=nil) { [data release]; }
    NSURLRequest* request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
    connection = [[NSURLConnection alloc]
                  initWithRequest:request delegate:self];
    //TODO error handling, what if connection is nil?
    
    //[self addActivity];
}

- (void)connection:(NSURLConnection *)theConnection

    didReceiveData:(NSData *)incrementalData {
    if (data==nil) {
        data =
        [[NSMutableData alloc] initWithCapacity:2048];
    }
    [data appendData:incrementalData];
}

- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
    
    [connection release];
    connection=nil;
    
    if ([[self subviews] count]>0) {
        [[[self subviews] objectAtIndex:0] removeFromSuperview];
    }
    
//    UIImageView* imageView = [[[UIImageView alloc] initWithImage:[UIImage imageWithData:data]] autorelease];
//    
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth || UIViewAutoresizingFlexibleHeight );
//    
//    [self addSubview:imageView];
//    imageView.frame = self.bounds;
//    [imageView setNeedsLayout];

    self.image = [UIImage imageWithData:data];
    
    self.contentMode = UIViewContentModeScaleAspectFit;
    self.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight );
    
    [self setNeedsLayout];
    [data release];
    data=nil;
    
    //[self removeActivity];
}

//- (UIImage*) image {
//    UIImageView* iv = [[self subviews] objectAtIndex:0];
//    return [iv image];
//}

-(void)addActivity{
    
	[self removeActivity];
    
	UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	CGRect aframe=CGRectMake(0, 0, 20, 20);
	activity.frame=aframe;

//	[ViewUtilities alignView:activity withView:self :BUCenterAlignMode :BUCenterAlignMode];
//	activity.tag=kAsyncActivityTAG;
    activity.tag = 100;
	[activity startAnimating];
	[self addSubview:activity];
}

-(void)removeActivity{
    
	UIActivityIndicatorView* activity = (UIActivityIndicatorView *)[self viewWithTag:100];
	if(activity!=nil){
		[activity stopAnimating];
		[activity removeFromSuperview];
		activity=nil;
	}
}

- (void)dealloc {
    [connection cancel];
    [connection release];
    [data release];
    [super dealloc];
}

@end
