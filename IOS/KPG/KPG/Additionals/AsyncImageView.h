//
//  AsyncImageView.h
//  MyKidsOnFilm
//
//  Created by osone on 9/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AsyncImageView : UIImageView {
    NSURLConnection* connection;
    NSMutableData* data;
}
+ (CGRect) GetInnerRect:(CGSize)contentsize :(CGSize)outsize;
- (void)loadImageFromURL:(NSURL*)url;
@end
