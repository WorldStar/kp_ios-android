//
//  WebViewController.h
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPGAppDelegate.h"

@interface WebViewController : UIViewController  {    
    KPGAppDelegate *appDelegate;
}

@property (strong, nonatomic) IBOutlet UIWebView *webView;

- (void)displayWebPage;

@end
