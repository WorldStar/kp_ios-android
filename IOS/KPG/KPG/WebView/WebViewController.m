//
//  WebViewController.m
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import "WebViewController.h"
#import "SWRevealViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];	
    // Do any additional setup after loading the view from its nib.
    
    SWRevealViewController *revealController = [self revealViewController];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    revealButtonItem.tintColor = [UIColor colorWithWhite:0.96 alpha:0.8f];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_back.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Vote";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    appDelegate = (KPGAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    
    [self displayWebPage];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    // Logs proves that at second load of view the object is NOT nil!!! But Check ->     - (void)eventOptionSheet!!
    [self displayWebPage];
    
    NSLog( @"%@: willAppear", NSStringFromSelector(_cmd));
}

- (void)displayWebPage {
    NSInteger currentPage = appDelegate.currentPage;
    NSString *fullURL = appDelegate.staticWebPages[currentPage];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    
    if( currentPage == 0 )
        self.title = @"About Us";
    else if( currentPage == 1 )
        self.title = @"Classes";
    else if( currentPage == 2 )
        self.title = @"Events";
    else if( currentPage == 3 )
        self.title = @"Contact Us";
    else
        self.title = @"Terms and Conditions";
}

@end
