//
//  KPGAppDelegate.m
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import "KPGAppDelegate.h"
#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "SideViewController.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "AsyncImageView.h"

@implementation KPGAppDelegate
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
//    self.mainView = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
//    self.rootNavi = [[UINavigationController alloc] initWithRootViewController:self.mainView];
//    [self.window addSubview:self.rootNavi.view];
    
    self.deviceId = [[UIDevice currentDevice].identifierForVendor UUIDString];
    
    NSLog(@"current device = %@",self.deviceId);
    
    /*  declar static webpages  */
    self.staticWebPages = @[
                            @"http://www.imanage.asia/kp/cms/pages/about.html",
                            @"http://www.imanage.asia/kp/cms/pages/classes.html",
                            @"http://www.imanage.asia/kp/cms/pages/events.html",
                            @"http://www.imanage.asia/kp/cms/pages/contactus.html",
                            @"http://www.imanage.asia/kp/cms/pages/terms.html",
                            @"http://www.imanage.asia/kp/cms/pages/policy.html"
                            ];
    
    /*  building navigation */
    MainViewController *mainView = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    SideViewController *sideView = [[SideViewController alloc] initWithNibName:@"SideViewController" bundle:nil];
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:mainView];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:sideView];
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    revealController.delegate = self;
    
    revealController.rearViewRevealWidth = 225;
    self.viewController = revealController;
    self.window.rootViewController = self.viewController;
    
    [self requestSend];
    [self.window makeKeyAndVisible];
    return YES;
}

+ (void) showWaitView :(NSString *) caption {
    KPGAppDelegate *appDelegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.loadingView = [[MBProgressHUD alloc] initWithView:appDelegate.window];
    [appDelegate.window addSubview:appDelegate.loadingView];
    appDelegate.loadingView.labelText = caption;
    [appDelegate.loadingView show:YES];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

+ (void) hideWaitView {
    KPGAppDelegate *appDelegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.loadingView != nil) {
        [appDelegate.loadingView hide:YES];
        [appDelegate.loadingView removeFromSuperview];
        appDelegate.loadingView = nil;
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}



- (void)requestSend{
    
    //----Server Request---
    NSString* serverUrl = [NSString stringWithFormat:@"%@/voteinfo.php",SERVER_URL];
    
    NSLog(@"%@", serverUrl);
    ASIFormDataRequest* request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:serverUrl]];
    [request setDelegate:self];
    [request setRequestMethod:@"GET"];
    [request setUseKeychainPersistence:YES];
    
    //[AppDelegate showWaitView:@"Loading..."];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [ASIHTTPRequest setShouldThrottleBandwidthForWWAN:YES];
    [ASIHTTPRequest throttleBandwidthForWWANUsingLimit:14800];
    [request setDidFinishSelector:@selector(requestDone:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setTimeOutSeconds:9999];
    [request setNumberOfTimesToRetryOnTimeout:2];
    [request startAsynchronous];
}


- (void)requestDone:(ASIFormDataRequest*)request {
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSLog(@"\n\n\n\nMain Screen->\n%@", [request responseString] );
    NSString* response = [request responseString];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    //[KPGAppDelegate hideWaitView];
    
    NSDecimalNumber *n_Status = [[response JSONValue] objectForKey:@"status"];
    NSLog(@"n_Status -> %@", n_Status);
    
    if ([n_Status isEqualToNumber:@1]) {
        //Get All Posts by Date

        delegate.eventsList = [[response JSONValue] objectForKey:@"content"];
        NSDictionary *dict_content = [delegate.eventsList objectForKey:@"1"];
        self.costPerVote = [dict_content objectForKey:@"costpervote"];
        self.costPerCurrency =[dict_content objectForKey:@"costpercurrency"];
        self.voteTermsConditions  = [dict_content objectForKey:@"votetermsconditions"];
        
        NSLog(@"voteTermsConditions -> %@", self.voteTermsConditions);

    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Faild" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}

- (void)requestFailed:(ASIFormDataRequest*)request {
    
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    NSLog(@"Failed");
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Server connnection failed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    return;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
