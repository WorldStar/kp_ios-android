
#import <Foundation/Foundation.h>

@class ImageSlideView;

@protocol ImageSlideViewDelegate <NSObject>

@optional

- (void)ImageSlideView:(ImageSlideView*)slideView didTapOnIndex:(int)index;
- (void)didChangPageToIndex:(int)nPage;

@end

@interface ImageSlideView : UIScrollView

@property (nonatomic, assign) id<ImageSlideViewDelegate> slideDelegate;

- (id)createWithFrame:(CGRect)frame URLPaths:(NSArray*)paths;

- (void)moveToPage:(int)nPage animated:(BOOL)bAnimated;

@end
