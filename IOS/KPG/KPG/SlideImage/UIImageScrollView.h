
#import <UIKit/UIKit.h>

@class UIImageScrollView;
@protocol UIImageScrollViewDelegate <NSObject>

@optional
- (void)UIImageScrollViewDidSingleTap:(UIImageScrollView*)imageView;

@end

@interface UIImageScrollView : UIScrollView <UIScrollViewDelegate>

@property (strong, nonatomic) UIImageView*  zoomView;
@property (nonatomic, assign) int           slideIndex;
@property (nonatomic, assign) id<UIImageScrollViewDelegate> tapDelegate;

@property (nonatomic, assign) BOOL bLoad;

- (void)setLocalImagePath:(NSString*)imgPath;
- (void)setImageURL:(NSURL*)imgURL;

- (void)restoreZoom;

@end
