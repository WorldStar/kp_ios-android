
#import "UIImageScrollView.h"
//#import "UIImageView+WebCache.h"

//#define ZOOM_STEP 1.25f
#define ZOOM_STEP 2.0f

@implementation UIImageScrollView
{
    CGSize _imageSize;
    CGPoint _pointToCenterAfterResize;
    CGFloat _scaleToRestoreAfterResize;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor blackColor];
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
        
        /*
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        doubleTap.numberOfTapsRequired = 2;
        [self addGestureRecognizer:doubleTap];
         */
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        singleTap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:singleTap];

        self.zoomView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame))];
        [self addSubview:self.zoomView];
        
        self.bLoad = NO;
    }
    
    return self;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)tap
{
    if( self.tapDelegate && [self.tapDelegate respondsToSelector:@selector(UIImageScrollViewDidSingleTap:)] )
        [self.tapDelegate UIImageScrollViewDidSingleTap:self];
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center
{
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    // At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    // As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [self frame].size.height / scale;
    zoomRect.size.width  = [self frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width * 0.5);
    zoomRect.origin.y    = center.y - (zoomRect.size.height * 0.5);
    
    return zoomRect;
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)tap
{
    if (self.zoomView) {
        CGPoint tapPoint = [tap locationInView:self.zoomView];
        float newScale = [self zoomScale] * ZOOM_STEP;
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:tapPoint];
        [self zoomToRect:zoomRect animated:YES];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    // center the zoom view as it becomes smaller than the size of the screen
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = self.zoomView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) * 0.5;
    } else {
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) * 0.5;
    } else {
        frameToCenter.origin.y = 0;
    }
    self.zoomView.frame = frameToCenter;
}

- (void)setFrame:(CGRect)frame
{
    BOOL sizeChanging = !CGSizeEqualToSize(frame.size, self.frame.size);
    
    if (sizeChanging) {
        [self prepareToResize];
    }
    
    [super setFrame:frame];
    
    if (sizeChanging) {
        [self recoverFromResizing];
    }
}

- (void)didFinishLoadLocalImage:(UIImage*)image
{
    self.zoomView.image = image;
    self.zoomView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    [self configureForImageSize:image.size];
}

- (void)removeActivityView:(UIView*)activityView
{
    [activityView removeFromSuperview];
}

- (void)setLocalImagePath:(NSString*)imgPath
{
    self.zoomScale = 1.0;
    self.zoomView.frame = self.bounds;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.zoomView.bounds;
    [activityIndicator startAnimating];
    
    [self.zoomView addSubview:activityIndicator];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        //UIImage*    image = [UIImage imageWithContentsOfFile:imgPath];
        UIImage*    image = [UIImage imageNamed:imgPath];
        [self performSelectorOnMainThread:@selector(didFinishLoadLocalImage:) withObject:image waitUntilDone:YES];
        [self performSelectorOnMainThread:@selector(removeActivityView:) withObject:activityIndicator waitUntilDone:YES];
    });

    /*UIImage*    image = [UIImage imageWithContentsOfFile:imgPath];
    self.zoomView.image = image;
    self.zoomView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    [self configureForImageSize:image.size];
     */
    
    self.bLoad = YES;
}

/*
- (void)setImageURL:(NSURL*)imgURL
{
    self.zoomScale = 1.0;
    self.zoomView.frame = self.bounds;

    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.zoomView.bounds;
    [activityIndicator startAnimating];
    
    [self.zoomView addSubview:activityIndicator];
    
    UIImageScrollView*  selfDelegate = self;
    
    [self.zoomView setImageWithURL:imgURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        if (!error && image) {
            [activityIndicator removeFromSuperview];
            selfDelegate.zoomView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
            [selfDelegate configureForImageSize:image.size];
        }
    }];
    
    self.bLoad = YES;
}*/

- (void)displayImage:(UIImage *)image
{
    self.zoomScale = 1.0;
    
    self.zoomView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    self.zoomView.image = image;
    
    [self configureForImageSize:image.size];
}

- (void)configureForImageSize:(CGSize)imageSize
{
    _imageSize = imageSize;
    self.contentSize = imageSize;
    [self setMaxMinZoomScalesForCurrentBounds];
    
    self.zoomScale = self.minimumZoomScale;
}

- (void)setMaxMinZoomScalesForCurrentBounds
{
    CGSize boundsSize = self.bounds.size;
    
    // calculate min/max zoomscale
    CGFloat xScale = boundsSize.width  / _imageSize.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = boundsSize.height / _imageSize.height;   // the scale needed to perfectly fit the image height-wise
    
    // fill width if the image and phone are both portrait or both landscape; otherwise take smaller scale
    BOOL imagePortrait = _imageSize.height > _imageSize.width;
    BOOL phonePortrait = boundsSize.height > boundsSize.width;
    CGFloat minScale = imagePortrait == phonePortrait ? xScale : MIN(xScale, yScale);
    
    // on high resolution screens we have double the pixel density, so we will be seeing every pixel if we limit the
    // maximum zoom scale to 0.5.
    CGFloat maxScale = 1.0 / [[UIScreen mainScreen] scale];
    
    // don't let minScale exceed maxScale. (If the image is smaller than the screen, we don't want to force it to be zoomed.)
    if (minScale > maxScale) {
        minScale = maxScale;
    }
    
    self.maximumZoomScale = 2.0f;//maxScale*2.0f;
    self.minimumZoomScale = 1.0f;//minScale;
}

#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.zoomView;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    [scrollView setZoomScale:scale + 0.01 animated:NO];
    [scrollView setZoomScale:scale animated:NO];
}

#pragma mark -

#pragma mark - Rotation support
- (void)prepareToResize
{
    CGPoint boundsCenter = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    _pointToCenterAfterResize = [self convertPoint:boundsCenter toView:_zoomView];
    
    _scaleToRestoreAfterResize = self.zoomScale;
    
    // If we're at the minimum zoom scale, preserve that by returning 0, which will be converted to the minimum
    // allowable scale when the scale is restored.
    if (_scaleToRestoreAfterResize <= self.minimumZoomScale + FLT_EPSILON) {
        _scaleToRestoreAfterResize = 0;
    }
}

- (void)recoverFromResizing
{
    [self setMaxMinZoomScalesForCurrentBounds];
    
    // Step 1: restore zoom scale, first making sure it is within the allowable range.
    CGFloat maxZoomScale = MAX(self.minimumZoomScale, _scaleToRestoreAfterResize);
    self.zoomScale = MIN(self.maximumZoomScale, maxZoomScale);
    
    // Step 2: restore center point, first making sure it is within the allowable range.
    
    // 2a: convert our desired center point back to our own coordinate space
    CGPoint boundsCenter = [self convertPoint:_pointToCenterAfterResize fromView:_zoomView];
    
    // 2b: calculate the content offset that would yield that center point
    CGPoint offset = CGPointMake(boundsCenter.x - self.bounds.size.width / 2.0,
                                 boundsCenter.y - self.bounds.size.height / 2.0);
    
    // 2c: restore offset, adjusted to be within the allowable range
    CGPoint maxOffset = [self maximumContentOffset];
    CGPoint minOffset = [self minimumContentOffset];
    
    CGFloat realMaxOffset = MIN(maxOffset.x, offset.x);
    offset.x = MAX(minOffset.x, realMaxOffset);
    
    realMaxOffset = MIN(maxOffset.y, offset.y);
    offset.y = MAX(minOffset.y, realMaxOffset);
    
    self.contentOffset = offset;
}

- (CGPoint)maximumContentOffset
{
    CGSize contentSize = self.contentSize;
    CGSize boundsSize = self.bounds.size;
    return CGPointMake(contentSize.width - boundsSize.width, contentSize.height - boundsSize.height);
}

- (CGPoint)minimumContentOffset
{
    return CGPointZero;
}

- (void)restoreZoom
{
    self.zoomScale = self.minimumZoomScale;
}

@end
