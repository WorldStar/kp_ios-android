
#import "ImageSlideView.h"
#import "UIImageScrollView.h"

@interface ImageSlideView () <UIScrollViewDelegate, UIImageScrollViewDelegate>
{
    NSArray*        arrayImgURLPaths;
    int             _nCurPage;
    
    NSMutableArray* _arrayItemViews;
}

@end

@implementation ImageSlideView

- (id)createWithFrame:(CGRect)frame URLPaths:(NSArray*)paths
{
    ImageSlideView* slideView = [[ImageSlideView alloc] initWithFrame:frame];
    
    [slideView createWithURLPaths:paths];

    return slideView;
}

- (void)createWithURLPaths:(NSArray*)paths
{
    arrayImgURLPaths = paths;
    
    _arrayItemViews = [[NSMutableArray alloc] initWithCapacity:0];
    
    int     nCount = (int)[paths count];
    CGSize  size = self.bounds.size;
    
    self.delegate = self;
    
    for( int i = 0; i < nCount; i++ )
    {
        CGRect  frame = self.bounds;
        
        frame.origin.x += i*size.width;

        UIImageScrollView*  itemView = [[UIImageScrollView alloc] initWithFrame:frame];
        [self addSubview:itemView];
        itemView.tapDelegate = self;
        itemView.slideIndex = i;

        [_arrayItemViews addObject:itemView];

        id  imgPath = [arrayImgURLPaths objectAtIndex:i];
        [itemView setLocalImagePath:imgPath];

        itemView.bounces = NO;
    }
    
    self.contentSize = CGSizeMake(size.width*nCount, size.height);
    self.bounces = NO;
    self.showsHorizontalScrollIndicator = NO;
    self.pagingEnabled = YES;
    
    _nCurPage = 0;
}

- (void)prepareOnePage:(int)pageIndex
{
    if( pageIndex < 0 || pageIndex >= [arrayImgURLPaths count] )
        return;
    
    UIImageScrollView*  itemView = (UIImageScrollView*)[_arrayItemViews objectAtIndex:pageIndex];
    if( itemView.bLoad == YES )
        return;

    id  imgPath = [arrayImgURLPaths objectAtIndex:pageIndex];
    
    if( [imgPath isKindOfClass:[NSURL class]] )
        [itemView setImageURL:imgPath];
    else
        [itemView setLocalImagePath:imgPath];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float   offset = self.contentOffset.x;
    int     nNewPage = (int)(offset / self.bounds.size.width);
    
    if( _nCurPage != nNewPage )
    {
        [self prepareOnePage:nNewPage];
        [self prepareOnePage:nNewPage-1];
        [self prepareOnePage:nNewPage+1];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView*)scrollView
{
    float   offset = self.contentOffset.x;
    int     nNewPage = (int)(offset / self.bounds.size.width);
    
    if( _nCurPage != nNewPage && _nCurPage < [_arrayItemViews count] )
    {
        UIImageScrollView*  itemView = (UIImageScrollView*)[_arrayItemViews objectAtIndex:_nCurPage];
        [itemView restoreZoom];
    }
    
    _nCurPage = nNewPage;

    if( self.slideDelegate && [self.slideDelegate respondsToSelector:@selector(didChangPageToIndex:)] )
        [self.slideDelegate didChangPageToIndex:_nCurPage];
}

- (void)UIImageScrollViewDidSingleTap:(UIImageScrollView*)imageView
{
    if( self.slideDelegate && [self.slideDelegate respondsToSelector:@selector(ImageSlideView:didTapOnIndex:)] )
        [self.slideDelegate ImageSlideView:self didTapOnIndex:imageView.slideIndex];
}

- (void)moveToPage:(int)nPage animated:(BOOL)bAnimated
{
    _nCurPage = nPage;
    
    [self prepareOnePage:_nCurPage];
    [self prepareOnePage:_nCurPage-1];
    [self prepareOnePage:_nCurPage+1];
    
    CGSize  size = self.bounds.size;
    CGRect  rect = CGRectMake( nPage*size.width, 0, size.width, size.height);
    
    [self scrollRectToVisible:rect animated:bAnimated];
}

@end
