//
//  ListViewController.m
//  KPG
//
//  Created by MyAdmin on 3/25/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import "ListViewController.h"
#import "KPGAppDelegate.h"
#import "SWRevealViewController.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "SelectedVoteViewController.h"
@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    SWRevealViewController *revealController = [self revealViewController];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    revealButtonItem.tintColor = [UIColor colorWithWhite:0.96 alpha:0.8f];
    self.navigationItem.leftBarButtonItem = revealButtonItem;

   
    self.title = NSLocalizedString(@"Vote", nil);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_back.png"] forBarMetrics:UIBarMetricsDefault];

    [KPGAppDelegate showWaitView:@"Loading..."];
    [self performSelector:@selector(requestSend) withObject:nil afterDelay:0.01f];
}

- (void)requestSend{

    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //----Server Request---
    NSString* serverUrl = [NSString stringWithFormat:@"%@/voteevent.php?reg_id=%@",SERVER_URL,delegate.deviceId];
    
    NSLog(@"%@", serverUrl);
    ASIFormDataRequest* request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:serverUrl]];
    [request setDelegate:self];
    [request setRequestMethod:@"GET"];
    [request setUseKeychainPersistence:YES];
    
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [ASIHTTPRequest setShouldThrottleBandwidthForWWAN:YES];
    [ASIHTTPRequest throttleBandwidthForWWANUsingLimit:14800];
    [request setDidFinishSelector:@selector(requestDone:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setTimeOutSeconds:9999];
    [request setNumberOfTimesToRetryOnTimeout:2];
    [request startAsynchronous];
}

- (void)requestDone:(ASIFormDataRequest*)request {
    
    //[self.scrollView removeFromSuperview];
    //self.scrollView = nil;
    //self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 63, 326, 505)];
    //self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 63, 326, 505)];
    //[self.view addSubview:self.scrollView];
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];

    NSString* response = [request responseString];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [KPGAppDelegate hideWaitView];
    
    NSDecimalNumber *n_Status = [[response JSONValue] objectForKey:@"status"];
    NSLog(@"n_Status -> %@", n_Status);
    
    if ([n_Status isEqualToNumber:@1]) {
        //Get All Posts by Date
        delegate.eventsList = [[response JSONValue] objectForKey:@"content"];
        totalEventsCount = (int)[delegate.eventsList count];
        
        for (int i = 1; i <= totalEventsCount; i++)
        {
            NSString *str_index = [NSString stringWithFormat:@"%d", i];
            NSDictionary *dict_content = [delegate.eventsList objectForKey:str_index];
            
            NSString *title = [dict_content objectForKey:@"title"];
            NSString *topVoteName = [dict_content objectForKey:@"topVoteName"];
            
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, (i-1) * 53, 320,53)];
            [button setImage:[UIImage imageNamed:@"vote_now_normal.png"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"vote_now_selected.png"] forState: UIControlStateSelected];
            [button setImage:[UIImage imageNamed:@"vote_now_selected.png"] forState: UIControlStateHighlighted];
            
            
            /* title Label */
            
            UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13]; //custom font
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 4, 210, 26)];
            UILabel *voteLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 23 , 70, 26)];
            UILabel *voteNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 23, 120, 26)];
            
            titleLabel.text = title;
            voteLabel.text = @"Top Vote:";
            voteNameLabel.text = topVoteName;
            
            titleLabel.font = titleFont;
            voteLabel.font = titleFont;
            voteNameLabel.font = titleFont;

            titleLabel.numberOfLines = 1;
            voteLabel.numberOfLines = 1;
            voteNameLabel.numberOfLines = 1;

            titleLabel.backgroundColor = [UIColor clearColor];
            voteLabel.backgroundColor = [UIColor clearColor];
            voteNameLabel.backgroundColor = [UIColor clearColor];
            
//            titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
//            titleLabel.adjustsFontSizeToFitWidth = YES;
//            titleLabel.adjustsLetterSpacingToFitWidth = YES;
//            titleLabel.minimumScaleFactor = 10.0f/12.0f;
//            titleLabel.clipsToBounds = YES;
//            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor whiteColor];
            voteLabel.textColor = [UIColor greenColor];
            voteNameLabel.textColor = [UIColor whiteColor];
            titleLabel.textAlignment = NSTextAlignmentLeft;
            
            [button addSubview:titleLabel];
            [button addSubview:voteLabel];
            [button addSubview:voteNameLabel];
            
            
            button.tag = i + 1000;
            [button addTarget:self action: @selector(buttonClicked:) forControlEvents: UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
         }
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 53 * (totalEventsCount+1))];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Faild" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}

- (void)buttonClicked:(UIButton*)sender
{    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    sender.selected = YES;

    NSInteger sender_id = sender.tag - 1000;
    delegate.currentEventIndex =sender_id;

    for( int i = 1; i <= totalEventsCount; i++ )
    {
        if( sender_id == i )
        {
            continue;
        }
        else
        {
            UIButton *button = (UIButton *)[self.scrollView viewWithTag:i+1000];
            button.selected = NO;
        }
    }
    
    SelectedVoteViewController* voteView = [[SelectedVoteViewController alloc] initWithNibName:@"SelectedVoteViewController" bundle:nil];
    [self.navigationController pushViewController:voteView animated:YES];
}

- (void)requestFailed:(ASIFormDataRequest*)request {
    
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    NSLog(@"Failed");
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Server connnection failed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    return;
}

- (void)setupScrollView:(UIScrollView*)scrMain {
    // we will add all images into a scrollView & set the appropriate size.
    
    // set the content size to 10 image width
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width, scrMain.frame.size.height*3)];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.title = NSLocalizedString(@"Vote", nil);
    self.navigationItem.title = @"Vote";
}

@end
