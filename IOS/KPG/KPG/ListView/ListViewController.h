//
//  ListViewController.h
//  KPG
//
//  Created by MyAdmin on 3/25/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController<UIScrollViewDelegate> {
    int totalEventsCount;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
