//
//  KPGAppDelegate.h
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "MBProgressHUD.h"


#define SERVER_URL @"http://imanage.asia/kp/ws"
#define IMAGE_SERVER_URL @"http://imanage.asia/kp/cms"

@class SWRevealViewController;

@interface KPGAppDelegate : UIResponder <UIApplicationDelegate, UINavigationControllerDelegate, SWRevealViewControllerDelegate>{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) NSString *deviceId;


//@property (strong, nonatomic) UINavigationController *rootNavi;
//@property (strong, nonatomic) MainViewController *mainView;
@property (strong, nonatomic) SWRevealViewController *viewController;

@property (strong, nonatomic) NSArray *staticWebPages;
@property (nonatomic) NSInteger currentPage;

@property (strong,nonatomic) NSDictionary  *eventsList;
@property (nonatomic) NSInteger currentEventIndex;

@property (nonatomic) int freeCount;
@property (nonatomic) int checkFree;
@property (strong,nonatomic) NSString *eventTitle;

@property (strong,nonatomic) NSDictionary  *voteItemList;
@property (nonatomic) NSInteger voteItemId;
@property (strong,nonatomic) NSString *voteItemName;
@property (nonatomic) NSInteger currentVoteItemIndex;
@property (strong,nonatomic) NSString *voteItemDescription;
@property (nonatomic) NSInteger percentVote;
@property (strong,nonatomic) NSString *voteItemImage;

//Share common vote info
@property (strong,nonatomic) NSString *voteTermsConditions;
@property (strong,nonatomic) NSDecimalNumber *costPerVote;
@property (strong,nonatomic) NSString *costPerCurrency;

@property (nonatomic,retain) MBProgressHUD* loadingView;
+ (void) showWaitView :(NSString *) caption;
+ (void) hideWaitView;

@end