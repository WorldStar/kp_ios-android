//
//  SideViewController.m
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import "SideViewController.h"
#import "SWRevealViewController.h"
#import "WebViewController.h"
#import "ListViewController.h"

@interface SideViewController ()

@end

@implementation SideViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = YES;
    
    self.btn_icon_aboutus.selected = NO;
    self.btn_icon_vote.selected = NO;
    self.btn_icon_setting.selected = NO;
    self.btn_icon_events.selected = NO;
    self.btn_icon_contact.selected = NO;
    self.btn_icon_terms.selected = NO;
    
    appDelegate = (KPGAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.currentPage > 100) {
         self.btn_icon_vote.selected = YES;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)OnClick_AboutUs:(id)sender {
    self.btn_icon_aboutus.selected = YES;
    self.btn_icon_vote.selected = NO;
    self.btn_icon_setting.selected = NO;
    self.btn_icon_events.selected = NO;
    self.btn_icon_contact.selected = NO;
    self.btn_icon_terms.selected = NO;
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    
    // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
    if ( ![frontNavigationController.topViewController isKindOfClass:[WebViewController class]] )
    {
        //set Current Page as AboutUs
        appDelegate.currentPage = 0; //index 0 indicates about Page
        
        WebViewController *webViewController = [[WebViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
    // Seems the user attempts to 'switch' to exactly the same controller he came from!
    else
    {
        if (appDelegate.currentPage != 0) {
            //set Current Page as AboutUs
            appDelegate.currentPage = 0; //index 0 indicates about Page
            
            WebViewController *webViewController = [[WebViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
            [revealController pushFrontViewController:navigationController animated:YES];
        } else {
            
            [revealController revealToggle:self];
        }
    }
    
}

- (IBAction)OnClick_Vote:(id)sender {
    self.btn_icon_aboutus.selected = NO;
    self.btn_icon_vote.selected = YES;
    self.btn_icon_setting.selected = NO;
    self.btn_icon_events.selected = NO;
    self.btn_icon_contact.selected = NO;
    self.btn_icon_terms.selected = NO;
    
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    
    // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
    if ( ![frontNavigationController.topViewController isKindOfClass:[ListViewController class]] )
    {
        ListViewController *listViewController = [[ListViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
    // Seems the user attempts to 'switch' to exactly the same controller he came from!
    else
    {
        [revealController revealToggle:self];
    }
}

- (IBAction)OnClick_Setting:(id)sender {
    self.btn_icon_aboutus.selected = NO;
    self.btn_icon_vote.selected = NO;
    self.btn_icon_setting.selected = YES;
    self.btn_icon_events.selected = NO;
    self.btn_icon_contact.selected = NO;
    self.btn_icon_terms.selected = NO;
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    
    // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
    if ( ![frontNavigationController.topViewController isKindOfClass:[WebViewController class]] )
    {
        //set Current Page as AboutUs
        appDelegate.currentPage = 1; //index 0 indicates about Page
        
        WebViewController *webViewController = [[WebViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
    // Seems the user attempts to 'switch' to exactly the same controller he came from!
    else
    {
        if (appDelegate.currentPage != 1) {
            //set Current Page as AboutUs
            appDelegate.currentPage = 1; //index 0 indicates about Page
            
            WebViewController *webViewController = [[WebViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
            [revealController pushFrontViewController:navigationController animated:YES];
        } else {
            
            [revealController revealToggle:self];
        }
    }
}

- (IBAction)onClick_Events:(id)sender {
    self.btn_icon_aboutus.selected = NO;
    self.btn_icon_vote.selected = NO;
    self.btn_icon_setting.selected = NO;
    self.btn_icon_events.selected = YES;
    self.btn_icon_contact.selected = NO;
    self.btn_icon_terms.selected = NO;
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    
    // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
    if ( ![frontNavigationController.topViewController isKindOfClass:[WebViewController class]] )
    {
        //set Current Page as Events
        appDelegate.currentPage = 2; //index 0 indicates about Page
        
        WebViewController *webViewController = [[WebViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
    // Seems the user attempts to 'switch' to exactly the same controller he came from!
    else
    {
        if (appDelegate.currentPage != 2) {
            //set Current Page as Events
            appDelegate.currentPage = 2; //index 0 indicates about Page
            
            WebViewController *webViewController = [[WebViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
            [revealController pushFrontViewController:navigationController animated:YES];
        } else {
            
            [revealController revealToggle:self];
        }
    }

}

- (IBAction)onClick_Contact:(id)sender {
    self.btn_icon_aboutus.selected = NO;
    self.btn_icon_vote.selected = NO;
    self.btn_icon_setting.selected = NO;
    self.btn_icon_events.selected = NO;
    self.btn_icon_contact.selected = YES;
    self.btn_icon_terms.selected = NO;
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    
    // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
    if ( ![frontNavigationController.topViewController isKindOfClass:[WebViewController class]] )
    {
        //set Current Page as ContactUs
        appDelegate.currentPage = 3; //index 0 indicates about Page
        
        WebViewController *webViewController = [[WebViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
    // Seems the user attempts to 'switch' to exactly the same controller he came from!
    else
    {
        if (appDelegate.currentPage != 3) {
            //set Current Page as ContactUs
            appDelegate.currentPage = 3; //index 0 indicates about Page
            
            WebViewController *webViewController = [[WebViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
            [revealController pushFrontViewController:navigationController animated:YES];
        } else {
            
            [revealController revealToggle:self];
        }
    }
}

- (IBAction)onClick_Terms:(id)sender {
    self.btn_icon_aboutus.selected = NO;
    self.btn_icon_vote.selected = NO;
    self.btn_icon_setting.selected = NO;
    self.btn_icon_events.selected = NO;
    self.btn_icon_contact.selected = NO;
    self.btn_icon_terms.selected = YES;
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    
    // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
    if ( ![frontNavigationController.topViewController isKindOfClass:[WebViewController class]] )
    {
        //set Current Page as TERMS
        appDelegate.currentPage = 4; //index 0 indicates about Page
        
        WebViewController *webViewController = [[WebViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
    // Seems the user attempts to 'switch' to exactly the same controller he came from!
    else
    {
        if (appDelegate.currentPage != 4) {
            //set Current Page as termS
            appDelegate.currentPage = 4; //index 0 indicates about Page
            
            WebViewController *webViewController = [[WebViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
            [revealController pushFrontViewController:navigationController animated:YES];
        } else {
            
            [revealController revealToggle:self];
        }
    }

}

@end
