//
//  SideViewController.h
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPGAppDelegate.h"

@interface SideViewController : UIViewController{
    KPGAppDelegate *appDelegate;
}
@property (strong, nonatomic) IBOutlet UIButton *btn_icon_aboutus;
@property (strong, nonatomic) IBOutlet UIButton *btn_icon_vote;
@property (strong, nonatomic) IBOutlet UIButton *btn_icon_setting;
@property (strong, nonatomic) IBOutlet UIButton *btn_icon_events;
@property (strong, nonatomic) IBOutlet UIButton *btn_icon_contact;
@property (strong, nonatomic) IBOutlet UIButton *btn_icon_terms;

- (IBAction)OnClick_AboutUs:(id)sender;
- (IBAction)OnClick_Vote:(id)sender;
- (IBAction)OnClick_Setting:(id)sender;
- (IBAction)onClick_Events:(id)sender;
- (IBAction)onClick_Contact:(id)sender;

- (IBAction)onClick_Terms:(id)sender;

@end
