//
//  VoteInfoViewController.h
//  KPG
//
//  Created by MyAdmin on 3/27/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface VoteInfoViewController : UIViewController <UITextFieldDelegate,UIAlertViewDelegate,SKPaymentTransactionObserver, SKProductsRequestDelegate>
{
    int m_voteEventId;
    int m_voteItemId;
    int m_pStatus;
    NSString *m_pEmail;
    NSString *m_pName;
    NSString *m_pReference;
    int m_pAmount;
    int m_pNumberofVote;
    
    UITextField * activeField;
    
    CGRect  scrollFrameSize;
    
    BOOL    bAddObserver;
    
    SKProductsRequest *product_request;
}

//in app purchase
@property (strong, nonatomic) NSString *productID;

//dismiss keyboard
@property (nonatomic, assign) id currentResponder;

@property (strong, nonatomic) IBOutlet UIButton *btn_votenow;
@property (strong, nonatomic) IBOutlet UILabel  *lbl_votenow;


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIImageView *votePageBack;
@property (strong, nonatomic) IBOutlet UILabel *divider;
@property (strong, nonatomic) IBOutlet UITextField *pName;
@property (strong, nonatomic) IBOutlet UITextField *pEmail;

- (IBAction)buttonClicked:(id)sender;

@end