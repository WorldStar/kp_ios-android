//
//  VoteInfoViewController.m
//  KPG
//
//  Created by MyAdmin on 3/27/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#import "VoteInfoViewController.h"
#import "KPGAppDelegate.h"
#import "ConfirmViewController.h"
#import "ListViewController.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "AsyncImageView.h"

@interface VoteInfoViewController ()

@end

@implementation VoteInfoViewController


- (void)SaveParam
{
    NSUserDefaults  *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setValue:self.pName.text forKey:@"vote_user_name"];
    [defaults setValue:self.pEmail.text forKey:@"vote_user_email"];
    
    [defaults synchronize];
}

- (void)LoadParam
{
    NSUserDefaults  *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString    *name = [defaults stringForKey:@"vote_user_name"];
    NSString    *email = [defaults stringForKey:@"vote_user_email"];
    
    if( name == nil )
        self.pName.text = @"";
    else
        self.pName.text = name;
    
    if( email == nil )
        self.pEmail.text = @"";
    else
        self.pEmail.text = email;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //for in app purchase
    //self.productID = @"KidPVOTE";
    self.productID = @"VoteForKids";

    bAddObserver = FALSE;
    
    //SETUP scrollview
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *str_index = [NSString stringWithFormat:@"%ld", (long)delegate.currentEventIndex];
    NSDictionary *dict_content = [delegate.eventsList objectForKey:str_index ];
    
    NSString *voteEventId_str = [dict_content objectForKey:@"voteEventid"];
    m_voteEventId = [voteEventId_str intValue];
    
    
    self.navigationItem.title = delegate.eventTitle;
    //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //self.navigationController.navigationBar.translucent = NO;
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(10, 5, 200, 34);
    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:15];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.shadowColor = [UIColor darkGrayColor];
    titleView.shadowOffset = CGSizeMake(0, -1);
    titleView.text = delegate.eventTitle;
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    self.scrollView.hidden = YES;

    [KPGAppDelegate showWaitView:@"Loading..."];
    [self performSelector:@selector(loadInfoFromServer) withObject:nil afterDelay:0.01f];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if( bAddObserver == TRUE )
    {
        [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
        bAddObserver = FALSE;
    }
}

- (void)loadInfoFromServer
{
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];

    // NSString *str_vote_item_id = [NSString stringWithFormat:@"%d", delegate.voteItemId];
    NSString *serverUrl = [NSString stringWithFormat:@"%@/pic/%@",IMAGE_SERVER_URL,delegate.voteItemImage];
    NSURL *imageURL = [NSURL URLWithString:serverUrl];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    /*NSData *imageData = [delegate.imageList objectForKey:str_vote_item_id];*/
    UIImage *image = [UIImage imageWithData:imageData];
    UIImage *newImage = [self imageWithImage:image scaledToWidth:self.view.frame.size.width];
    [self.votePageBack setFrame:CGRectMake(self.votePageBack.frame.origin.x, self.votePageBack.frame.origin.y, self.votePageBack.frame.size.width, newImage.size.height)];
    
    [self.votePageBack setImage:newImage];

    //[self.btn_votenow removeFromSuperview];
    //[self.scrollView removeFromSuperview];
    //[self.view addSubview:self.scrollView];
    
    int up_space = newImage.size.height*7/12;
    /* Backgrounds */
    
    /* title Label */
    //UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15]; //custom font
    UIFont *titleFont = [UIFont systemFontOfSize:15]; //custom font
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, up_space+48, 235, 23)];
    titleLabel.text = delegate.voteItemName;
    titleLabel.font = titleFont;
    titleLabel.numberOfLines = 1;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.backgroundColor = [UIColor clearColor];
    
    UILabel *ageLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, up_space+67, 235, 23)];
    ageLabel.text = delegate.eventTitle;
    ageLabel.font = titleFont;
    ageLabel.numberOfLines = 1;
    ageLabel.textColor = [UIColor whiteColor];
    ageLabel.textAlignment = NSTextAlignmentLeft;
    ageLabel.backgroundColor = [UIColor clearColor];
    
    
    /* progres bar edge */
    UIImage *imageEdge = [UIImage imageNamed:@"icon_bar_enabled.png"];
    UIImageView *barEdgeView = [[UIImageView alloc] initWithImage:imageEdge];
    [barEdgeView setFrame:CGRectMake(10, up_space+95, 182, 8)];
    
    if( delegate.percentVote > 0 )
    {
        UIView  *progressView = [[UIView alloc] initWithFrame:CGRectMake(3, 2, 176*delegate.percentVote/100, 4)];
        progressView.backgroundColor = [UIColor greenColor];
        [barEdgeView addSubview:progressView];
    }
    
    /*progress Bar */
    UIProgressView *personProgressBar = [[UIProgressView alloc] initWithFrame:CGRectMake(3, 3, 177, 8)];
    [personProgressBar setProgressViewStyle:UIProgressViewStyleBar];
    [personProgressBar setProgressTintColor: [UIColor colorWithRed:0 green:255 blue:0 alpha:1]];
    [personProgressBar setTrackTintColor: [UIColor clearColor]];
    personProgressBar.progress = (float)(delegate.percentVote/100.0f);
    [personProgressBar setNeedsDisplay];
    [barEdgeView addSubview:personProgressBar];
    [personProgressBar setHidden:YES];
    
    /* title Label */
    UILabel *percentLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, up_space+86, 60, 23)];
    percentLabel.text = [NSString stringWithFormat:@"%ld%%",(long)delegate.percentVote];
    percentLabel.font = titleFont;
    percentLabel.numberOfLines = 1;
    percentLabel.textColor = [UIColor whiteColor];
    percentLabel.textAlignment = NSTextAlignmentLeft;
    percentLabel.backgroundColor = [UIColor clearColor];
    
    /* Description Label */
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, up_space+116, 235, 300)];
    descriptionLabel.text = delegate.voteItemDescription;
    NSLog(@"CURRENT DESCRIPTION - > %@",descriptionLabel.text);
    descriptionLabel.font = titleFont;
    descriptionLabel.textColor = [UIColor whiteColor];
    descriptionLabel.textAlignment = NSTextAlignmentLeft;
    descriptionLabel.numberOfLines = 100;
    [descriptionLabel sizeToFit];
    descriptionLabel.backgroundColor = [UIColor clearColor];
    
    CGSize maximumLabelSize = CGSizeMake(225, 9999);
    CGSize expectedSize = [descriptionLabel sizeThatFits:maximumLabelSize];
    CGRect newFrame = descriptionLabel.frame;
    newFrame.size.height = expectedSize.height;
    //descriptionLabel.frame = newFrame;
    
    int firstHalfHeight = titleLabel.frame.size.height + ageLabel.frame.size.height + percentLabel.frame.size.height + descriptionLabel.frame.size.height;
    
    int lastPos = (int)descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height - up_space;
    
    
    NSLog(@"FIRST half size = %d, lastPos = %d", firstHalfHeight,lastPos);
    
    /* scrollview background Image */
    UIImage *base_back_image = [UIImage imageNamed:@"term_back.png"];
    UIImageView *base_back_view = [[UIImageView alloc] initWithImage:base_back_image];
    [base_back_view setFrame:CGRectMake(0, up_space+41, 275, lastPos+15-41)];
    
    [self.divider setFrame:CGRectMake(self.divider.frame.origin.x, up_space + 110, self.divider.frame.size.width, self.divider.frame.size.height)];
    
    [self.scrollView addSubview:base_back_view];
    [self.scrollView addSubview:titleLabel];
    [self.scrollView addSubview:ageLabel];
    [self.scrollView addSubview:barEdgeView];
    [self.scrollView addSubview:percentLabel];
    [self.scrollView addSubview:descriptionLabel];
    [self.scrollView addSubview:self.divider];
    
    //from here second half;
    
    int lastHalfStart = lastPos + up_space + 15;
    
    /* Vote Now Button */
    [self.btn_votenow setFrame:CGRectMake(self.btn_votenow.frame.origin.x, lastHalfStart+15, self.btn_votenow.frame.size.width, self.btn_votenow.frame.size.height)];
    
    [self.lbl_votenow setFrame:CGRectMake(self.btn_votenow.frame.origin.x+10, lastHalfStart+15, self.btn_votenow.frame.size.width-20, self.btn_votenow.frame.size.height)];
    
    NSString *vote_now_buttton_tile = [NSString stringWithFormat:@"Vote now!   (Free vote : %d)", delegate.freeCount];
    
    //[self.btn_votenow setTitle:vote_now_buttton_tile forState:UIControlStateNormal];
    //[self.btn_votenow setTitle:vote_now_buttton_tile forState:UIControlStateSelected];
    //[self.btn_votenow setTitle:vote_now_buttton_tile forState:UIControlStateHighlighted];
    [self.btn_votenow setTitle:@"" forState:UIControlStateNormal];
    self.lbl_votenow.text = vote_now_buttton_tile;

    //self.voteNow.adjustsImageWhenHighlighted = YES;
    [self.pName setFrame:CGRectMake(self.pName.frame.origin.x, lastHalfStart + 70, self.pName.frame.size.width, self.pName.frame.size.height)];
    
    [self.pEmail setFrame:CGRectMake(self.pEmail.frame.origin.x, lastHalfStart + 108, self.pEmail.frame.size.width, self.pEmail.frame.size.height)];
    
    
    UILabel *termLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, lastHalfStart+150, 235, 300)];
    termLabel.text = delegate.voteTermsConditions;
    termLabel.font = titleFont;
    termLabel.textColor = [UIColor whiteColor];
    termLabel.textAlignment = NSTextAlignmentLeft;
    termLabel.backgroundColor = [UIColor clearColor];
    
    CGSize term_maximumLabelSize = CGSizeMake(225, 9999);
    CGSize term_expectedSize = [termLabel sizeThatFits:term_maximumLabelSize];
    CGRect term_newFrame = termLabel.frame;
    term_newFrame.size.height = term_expectedSize.height;
    //termLabel.frame = term_newFrame;
    termLabel.numberOfLines = 100;
    [termLabel sizeToFit];
    
    lastPos = (int)termLabel.frame.origin.y + termLabel.frame.size.height - lastHalfStart;
    
    NSLog(@"Last half lastPos = %d", lastPos);
    
    UIImage *base_term_image = [UIImage imageNamed:@"term_back.png"];
    UIImageView *base_term_view = [[UIImageView alloc] initWithImage:base_term_image];
    [base_term_view setFrame:CGRectMake(0, lastHalfStart+57, 275, lastPos - 57 + 15)];
    
    
    
    //[self.scrollView addSubview:self.btn_votenow];
    [self.scrollView addSubview:base_term_view];
    [self.scrollView addSubview:self.pName];
    [self.scrollView addSubview:self.pEmail];
    [self.scrollView addSubview:termLabel];
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, lastHalfStart+lastPos+30)];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnTap:)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:singleTap];
    
    [self registerForKeyboardNotifications];
    
    [KPGAppDelegate hideWaitView];
    
    self.scrollView.hidden = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self LoadParam];
}

- (IBAction)buttonClicked:(id)sender
{
    [self.view endEditing:YES];
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    if( [self checkInputValues] )
    {
        [self SaveParam];
        
        m_pEmail = self.pEmail.text;
        m_pName = self.pName.text;
        
        if( delegate.checkFree == 0 )
        {
            [self PurchaseFree];
        }
        else
        {
            [self getProductInfo];
        }
    }
}

- (void)PurchaseFree
{
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *str_index = [NSString stringWithFormat:@"%ld", (long)delegate.currentEventIndex];
    NSDictionary *dict_content = [delegate.eventsList objectForKey:str_index ];
        
    NSString *voteEventId_str = [dict_content objectForKey:@"voteEventid"];
    m_voteEventId = [voteEventId_str intValue];
    m_voteItemId = (int)delegate.voteItemId;
        
    m_pStatus = 3;
        
    m_pAmount = 0;
    m_pReference = @"";
    m_pNumberofVote = 1;
        
    [self requestSend];
}

- (void)PurchaseInApp
{
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *str_index = [NSString stringWithFormat:@"%ld", (long)delegate.currentEventIndex];
    NSDictionary *dict_content = [delegate.eventsList objectForKey:str_index ];
    
    NSString *voteEventId_str = [dict_content objectForKey:@"voteEventid"];
    m_voteEventId = [voteEventId_str intValue];
    m_voteItemId = (int)delegate.voteItemId;
    
    m_pStatus = 1;
    
    m_pAmount = 2;
    m_pReference = @"";
    m_pNumberofVote = 1;
    
    [self requestSend];
}

- (void)PurchaseOK{
    ConfirmViewController* confirmView = [[ConfirmViewController alloc] initWithNibName:@"ConfirmViewController" bundle:nil];
    [self.navigationController pushViewController:confirmView animated:YES];
}


- (void)PurchaseFail{
    SWRevealViewController *revealController = self.revealViewController;
    
    ListViewController *listViewController = [[ListViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    [revealController pushFrontViewController:navigationController animated:YES];
}

- (void)requestSend
{
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];

    //----Server Request---
    NSString* serverUrl = [NSString stringWithFormat:@"%@/voteresult.php?v_event_id=%d&v_item_id=%d&reg_id=%@&pstatus=%d&premark=%@&pname=%@&preference=%@&pamount=%d&pcurrency=SGD&numberofvote=%d&ostype=IOS&pemail=%@",SERVER_URL,m_voteEventId,m_voteItemId,delegate.deviceId,m_pStatus,m_pEmail,m_pName,m_pReference,m_pAmount,m_pNumberofVote, m_pEmail];
    
    NSLog(@"VOTE SUBMIT URL = %@",serverUrl);
    
    ASIFormDataRequest* request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:serverUrl]];
    
    [request setDelegate:self];
    [request setRequestMethod:@"GET"];
    [request setUseKeychainPersistence:YES];
    
    [KPGAppDelegate showWaitView:@"Loading..."];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [ASIHTTPRequest setShouldThrottleBandwidthForWWAN:YES];
    [ASIHTTPRequest throttleBandwidthForWWANUsingLimit:14800];
    [request setDidFinishSelector:@selector(requestDone:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setTimeOutSeconds:9999];
    [request setNumberOfTimesToRetryOnTimeout:2];
    [request startAsynchronous];
}

- (void)requestDone:(ASIFormDataRequest*)request
{
    NSString* response = [request responseString];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [KPGAppDelegate hideWaitView];
    
    NSDecimalNumber *n_Status = [[response JSONValue] objectForKey:@"status"];
    if ([n_Status isEqualToNumber:@1]) {
        
    } else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Faild"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }

    [self PurchaseOK];
}

- (BOOL)checkInputValues {
    
    if( [self.pName.text  isEqual: @""] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Kids Performing"
                                                     message:@"Please enter your name"
                                                    delegate:self cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        av.tag = 2000;
        [av show];
        return NO;
    }
    
    if( [self isValidEmail:self.pEmail.text] == NO )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Kids Performing"
                                                      message:@"Please enter valid email address."
                                                     delegate:self cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        av.tag = 2001;
        [av show];
        return NO;
    }

    return YES;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 2000)
        [self.pName becomeFirstResponder];
    else if(alertView.tag == 2001)
        [self.pEmail becomeFirstResponder];
    else if(alertView.tag == 2003)
        [self PurchaseOK];
    else if(alertView.tag == 2004)
        [self PurchaseFail];
}

-(BOOL)isValidEmail: (NSString *)checkString {
    
    //NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = laxString;
    //NSString *emailRegex= stricterFilterString :
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField == self.pEmail) {
        
       /* NSString *regEx = @"[0-9]{3}-[0-9]{2}-[0-9]{4}";
        NSRange r = [textField.text rangeOfString:regEx options:NSRegularExpressionSearch];
        if (r.location == NSNotFound) {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Email error"
                                                          message:@"Enter valid email address. for example 'john@gmail.com'"
                                                         delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [av show];
            return NO;
        }*/
    }
    return YES;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField {
    [textField resignFirstResponder];
    return YES;
}

//Implement the below delegate method:

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.currentResponder = textField;
    activeField = textField;
}

//Implement resignOnTap:

- (void)resignOnTap:(id)iSender {
    [self.currentResponder resignFirstResponder];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeField.superview.frame;
    scrollFrameSize = activeField.superview.frame;
    bkgndRect.size.height -= kbSize.height;
    [activeField.superview setFrame:bkgndRect];
    [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    //[self.scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    [self.scrollView setFrame:scrollFrameSize];
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    NSLog(@"SCALEFACTOR ========= %f",scaleFactor);
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)getProductInfo
{
    if( [SKPaymentQueue canMakePayments] )
    {
        product_request = [[SKProductsRequest alloc]
                                      initWithProductIdentifiers:
                                      [NSSet setWithObject:self.productID]];
        product_request.delegate = self;
        [KPGAppDelegate showWaitView:@"Connecting to appstore..."];
        [product_request start];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"In App Purchase Error"
                                                            message:@"Please enable In App Purchase in Settings"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
}


#pragma mark -
#pragma mark SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    [KPGAppDelegate hideWaitView];
    
    NSArray *products = response.products;
    
    if( products.count != 0 )
    {
        if( bAddObserver == FALSE )
        {
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            bAddObserver = TRUE;
        }
        
        SKPayment *payment = [SKPayment paymentWithProduct:products[0]];
        //NSString *ss1 = payment.applicationUsername;
        //NSString *ss2 = payment.productIdentifier;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    } else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"In App Purchase Error"
                                                            message:@"Product not found"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    /*
    products = response.invalidProductIdentifiers;
    
    for (SKProduct *product in products)
    {
        NSLog(@"Product not found: %@", product);
    }*/
}

#pragma mark -
#pragma mark SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    [KPGAppDelegate hideWaitView];

    UIAlertView *av;
    NSInteger   errorCode;
    NSError     *error;
    
    for( SKPaymentTransaction *transaction in transactions )
    {
        NSString *sss = transaction.payment.productIdentifier;
        
        sss = sss;

        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self PurchaseInApp];
                break;
                
            case SKPaymentTransactionStateFailed:
                error = transaction.error;
                errorCode = transaction.error.code;
                if ( errorCode != SKErrorPaymentCancelled)
                {
                    av = [[UIAlertView alloc] initWithTitle:@"In App Purchase Error"
                                                message:transaction.error.localizedDescription//@"Purchase Failed."
                                               delegate:nil//self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];

                    av.tag = 2004; //purchase fail
                    [av show];
                }
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

//http://www.raywenderlich.com/21081/
//http://stackoverflow.com/questions/19614155/how-can-i-confirm-whether-the-in-app-purchase-actually-worked-on-a-test-itunes-c

@end
