//
//  ConfirmViewController.h
//  KPG
//
//  Created by MyAdmin on 3/28/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btn_closeVote;
- (IBAction)btn_closeVote_click:(id)sender;

@end
