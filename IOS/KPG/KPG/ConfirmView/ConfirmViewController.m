//
//  ConfirmViewController.m
//  KPG
//
//  Created by MyAdmin on 3/28/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import "ConfirmViewController.h"
#import "ListViewController.h"
#import "SWRevealViewController.h"
#import "KPGAppDelegate.h"

@interface ConfirmViewController ()

@end

@implementation ConfirmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.hidesBackButton = YES;
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationItem.title = delegate.eventTitle;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btn_closeVote_click:(id)sender {
    
    SWRevealViewController *revealController = self.revealViewController;

    ListViewController *listViewController = [[ListViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    [revealController pushFrontViewController:navigationController animated:YES];

}

@end
