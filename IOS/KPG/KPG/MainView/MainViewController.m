//
//  MainViewController.m
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "ListViewController.h"
#import "KPGAppDelegate.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize scr;

- (bool)is4InchScreen
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    if( screenHeight >= 568 )
        return YES;

    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
//    self.navigationController.navigationBarHidden = YES;

    SWRevealViewController *revealController = [self revealViewController];

    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];

    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon.png"]
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:revealController
                                                                        action:@selector(revealToggle:)];
    revealButtonItem.tintColor = [UIColor colorWithWhite:0.96 alpha:0.8f];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    self.title = NSLocalizedString(@"Vote", nil);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_back.png"] forBarMetrics:UIBarMetricsDefault];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    /*
    //Main Slider Implementation
    numOfPages = 3;
    [scr setContentSize:CGSizeMake(scr.frame.size.width*numOfPages, scr.frame.size.height)];
    
    NSLog(@"Current view height->>>>>>>%d", (int)self.view.frame.size.height);
    
    
    for (int i = 0; i < numOfPages; i++) {
        
        CGRect slideImageRect;
        
        if ([self is4InchScreen]){
            slideImageRect = CGRectMake(i * scr.frame.size.width, 0, 320, scr.frame.size.height - 62);
        } else {
            slideImageRect = CGRectMake(i * scr.frame.size.width, 0, 320, scr.frame.size.height);
        }
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:slideImageRect];
        
        
        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"image_slide%d.png",i+1]];
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.tag = 564 + i;
        
        NSLog(@"ImageView Height = %f",imageView.frame.size.height);
        [imageView setAutoresizingMask:scr.autoresizingMask];
        
        [scr addSubview:imageView];
    }
    
    NSLog(@"Current Scroll View Offset: %f, %f", scr.contentOffset.x, scr.contentOffset.y);
    //[scr setContentOffset:CGPointMake(0, 64) animated:FALSE];
    
    NSLog(@"Scroll Size : %f-%f", scr.contentSize.height, scr.bounds.size.height);
    
    [self.view bringSubviewToFront:self.bottomPageControl];
    [self.view bringSubviewToFront:self.btn_next];
    [self.view bringSubviewToFront:self.btn_prev];
    [self.view bringSubviewToFront:self.btn_vote_now];

    if ([self is4InchScreen]){
        [scr setFrame:CGRectMake(0, 0, 320, 480)];
    } else {
        [scr setContentOffset:CGPointMake(0, 62) animated:NO];
    }
    
    [self checkPageForVote];
    */
    
    bInitGallery = FALSE;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if( bInitGallery == FALSE )
    {
        [self initGallery];
        [self checkPageForVote];
        bInitGallery = TRUE;
    }
}

- (void)initGallery
{
    NSMutableArray*     imageURLs = [[NSMutableArray alloc] initWithCapacity:0];
    
    numOfPages = 3;
    for( int i = 0; i < numOfPages; i++ )
    {
        NSString *fileName = [NSString stringWithFormat:@"image_slide%d.png",i+1];
        [imageURLs addObject:fileName];
    }

    photoSlider = [[ImageSlideView alloc] createWithFrame:scr.bounds URLPaths:imageURLs];
    [scr addSubview:photoSlider];
    [photoSlider moveToPage:0 animated:NO];
    photoSlider.delegate = self;
    
    photoSlider.bounces = NO;
}

- (void)didChangPageToIndex:(int)nPage
{
    self.bottomPageControl.currentPage = nPage;
    [self checkPageForVote];
}

- (IBAction)pageChanged:(id)sender {
    NSInteger   nPage = self.bottomPageControl.currentPage;
    float       newX = scr.frame.size.width * nPage;

    [scr setContentOffset:CGPointMake(newX, 0) animated:YES];
    
    NSLog(@"Current Scroll View Offset: %f, %f", scr.contentOffset.x, scr.contentOffset.y);
    NSLog(@"Scroll Size : %f-%f", scr.contentSize.height, scr.bounds.size.height);
    
    [self checkPageForVote];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int nPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    NSLog(@"nPage -> %d", nPage);
    self.bottomPageControl.currentPage = nPage;
    //NSLog(@"Current Scroll View Offset: %f, %f", scr.contentOffset.x, scr.contentOffset.y);
    //[scr setContentOffset:CGPointMake(scrollView.contentOffset.x, -64) animated:NO];
    NSLog(@"Scroll Size : %f-%f", scr.contentSize.height, scr.bounds.size.height);
    [self checkPageForVote];
}


- (IBAction)voteNowClick:(id)sender {
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    // We know the frontViewController is a NavigationController
    ListViewController *listViewController = [[ListViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    [revealController pushFrontViewController:navigationController animated:YES];
 
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    delegate.currentPage = 1000;
}

- (IBAction)prevClicked:(id*)sender {
    NSInteger nPage = self.bottomPageControl.currentPage;
    
    if( nPage > 0 ) {
        //float newX = scr.contentOffset.x  - scr.frame.size.width;
        //[scr setContentOffset:CGPointMake(newX, 0) animated:YES];
        self.bottomPageControl.currentPage--;
        [photoSlider moveToPage:(int)self.bottomPageControl.currentPage animated:YES];
    }

    [self checkPageForVote];
}


- (IBAction)nextClicked:(id*)sender{
    NSInteger nPage = self.bottomPageControl.currentPage;
    
    if( nPage < numOfPages -1 ) {
        //float newX = scr.contentOffset.x  + scr.frame.size.width;
        //[scr setContentOffset:CGPointMake(newX, 0) animated:YES];
        self.bottomPageControl.currentPage++;
        [photoSlider moveToPage:(int)self.bottomPageControl.currentPage animated:YES];
    }

    NSLog(@"Scroll Size : %f-%f", scr.contentSize.height, scr.bounds.size.height);
    [self checkPageForVote];
}


-(void) checkPageForVote
{
    NSInteger nPage = self.bottomPageControl.currentPage;
    if( nPage != 0 ) {
        self.btn_vote_now.hidden = YES;
    } else {
        self.btn_vote_now.hidden = NO;
    }
    
    if (nPage == 0){
        self.btn_prev.hidden = YES;
    }else{
        self.btn_prev.hidden = NO;
    }

    if (nPage == 2){
        self.btn_next.hidden = YES;
    }else{
        self.btn_next.hidden = NO;
    }
}

@end
