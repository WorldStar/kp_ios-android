//
//  MainViewController.h
//  KPG
//
//  Created by MyAdmin on 3/24/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "ImageSlideView.h"

@interface MainViewController : UIViewController <UIScrollViewDelegate, SWRevealViewControllerDelegate> {
    NSInteger   numOfPages;
    BOOL        bInitGallery;
    
    ImageSlideView*       photoSlider;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scr;
@property (strong, nonatomic) IBOutlet UIPageControl *bottomPageControl;

- (IBAction)pageChanged:(id)sender;

//- (void)setupScrollView:(UIScrollView*)scrMain ;

@property (strong, nonatomic) IBOutlet UIButton *btn_prev;
@property (strong, nonatomic) IBOutlet UIButton *btn_next;
@property (strong, nonatomic) IBOutlet UIButton *btn_vote_now;
- (IBAction)voteNowClick:(id)sender;

- (IBAction)prevClicked:(id*)sender;
- (IBAction)nextClicked:(id*)sender;
@end
