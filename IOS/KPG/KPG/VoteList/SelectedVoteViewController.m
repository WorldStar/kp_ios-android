//
//  SelectedVoteViewController.m
//  KPG
//
//  Created by MyAdmin on 3/26/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import "SelectedVoteViewController.h"
#import "KPGAppDelegate.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "AsyncImageView.h"

#import "VoteInfoViewController.h"

@interface SelectedVoteViewController ()

@end

@implementation SelectedVoteViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *str_index = [NSString stringWithFormat:@"%ld", (long)delegate.currentEventIndex];
    NSDictionary *dict_content = [delegate.eventsList objectForKey:str_index ];
    
    NSString *voteEventId_str = [dict_content objectForKey:@"voteEventid"];
    m_voteEventId = [voteEventId_str intValue];
    
    m_backgroundUrl = [dict_content objectForKey:@"backgroundPathUrl"];
    NSString* free_count = [dict_content objectForKey:@"freecount"];
    delegate.freeCount = [free_count intValue];
    
    NSString* check_free = [dict_content objectForKey:@"checkfree"];
    delegate.checkFree = [check_free intValue];
    
    NSString *title =[dict_content objectForKey:@"title"];
    delegate.eventTitle = title;

    [self.imageView setBackgroundColor:[UIColor blackColor]];

    self.navigationItem.title = title;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.topItem.title = @"Back";
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(10, 5, 200, 34);
    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:15];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.shadowColor = [UIColor darkGrayColor];
    titleView.shadowOffset = CGSizeMake(0, -1);
    titleView.text = title;
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
  

    self.navigationItem.titleView = _headerTitleSubtitleView;
    leftMargin = 10;
    
    //[self requestSend];
    [KPGAppDelegate showWaitView:@"Loading..."];
    [self performSelector:@selector(requestSend) withObject:nil afterDelay:0.1f];
}

- (void)requestSend
{
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString* serverUrl = nil;
    
#if 1
    serverUrl = [NSString stringWithFormat:@"%@/voteeventbackground/%@",IMAGE_SERVER_URL,m_backgroundUrl];
    NSURL * imageURL = [NSURL URLWithString:serverUrl];
    NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage * image = [UIImage imageWithData:imageData];

    [self.imageView setImage:image];
#endif
    
    //----Server Request---
    serverUrl = [NSString stringWithFormat:@"%@/voteitem.php?v_event_id=%d&reg_id=%@",SERVER_URL,m_voteEventId,delegate.deviceId];
    ASIFormDataRequest* request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:serverUrl]];
    
    [request setDelegate:self];
    [request setRequestMethod:@"GET"];
    [request setUseKeychainPersistence:YES];
    
    //[KPGAppDelegate showWaitView:@"Loading..."];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [ASIHTTPRequest setShouldThrottleBandwidthForWWAN:YES];
    [ASIHTTPRequest throttleBandwidthForWWANUsingLimit:14800];
    [request setDidFinishSelector:@selector(requestDone:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setTimeOutSeconds:9999];
    [request setNumberOfTimesToRetryOnTimeout:2];
    [request startAsynchronous];
}


- (void)requestDone:(ASIFormDataRequest*)request
{
    //[self.scrollView removeFromSuperview];
    //self.scrollView = nil;
    //self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 63, 326, self.view.frame.size.height-64)];
    //[self.view addSubview:self.scrollView];
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString* response = [request responseString];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [KPGAppDelegate hideWaitView];
    
    NSDecimalNumber *n_Status = [[response JSONValue] objectForKey:@"status"];    
    if ([n_Status isEqualToNumber:@1]) {
        //Get All Posts by Date
        NSDictionary *itemList = [[response JSONValue] objectForKey:@"content"];
        m_voteItemCount = (int)[itemList count];
        
        
        //calc total vote count
        int totalNumOfVotes = 0;
        for (int i = 1; i <= m_voteItemCount; i++)
        {
            NSString *str_index = [NSString stringWithFormat:@"%d", i];
            NSDictionary *dict_content = [itemList objectForKey:str_index];
            
            NSString *str_numOfVotes = [dict_content objectForKey:@"NumbersofVote"];
            totalNumOfVotes = totalNumOfVotes + [str_numOfVotes intValue];
        }
        
        delegate.voteItemList = itemList;
        
        if (totalNumOfVotes == 0) totalNumOfVotes = 1;
        
        //[delegate.imageList removeAllObjects];
        
        for (int i = 1; i <= m_voteItemCount; i++)
        {
            NSString *str_index = [NSString stringWithFormat:@"%d", i];
            NSDictionary *dict_content = [itemList objectForKey:str_index];
            NSString *title = [dict_content objectForKey:@"name"];
            NSString *str_numOfVotes = [dict_content objectForKey:@"NumbersofVote"];
            
            NSString *status_str = [dict_content objectForKey:@"status"];
            int item_status = [status_str intValue];
            
            int percent = (int)([str_numOfVotes intValue] * 100 / totalNumOfVotes);
            //button creation
            
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, (i-1) * 53, 320,53 )];
            [button setTitle:title forState:UIControlStateNormal];
            UIImage *btnImage_normal;
            if (item_status == 0 ) {
                btnImage_normal = [UIImage imageNamed:@"item_normal"];
            } else {
                btnImage_normal = [UIImage imageNamed:@"item_disabled.png"];
            }
            
            [button setImage:btnImage_normal forState: UIControlStateNormal];
            UIImage *btnImage_selected = [UIImage imageNamed:@"item_selected.png"];
            [button setImage:btnImage_selected forState: UIControlStateSelected];
            [button setImage:btnImage_selected forState: UIControlStateHighlighted];
            if (item_status == 0) {
                button.enabled = YES;
            }else {
                button.enabled = NO;
            }

            /* title Label */
            UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13]; //custom font
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin + 50, 5, 200, 26)];
            titleLabel.text = title;
            titleLabel.font = titleFont;
            titleLabel.numberOfLines = 1;
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.textAlignment = NSTextAlignmentLeft;
            titleLabel.backgroundColor = [UIColor clearColor];
            
            /* progres bar edge */
            UIImage *imageEdge = [UIImage imageNamed:@"icon_bar_enabled.png"];
            UIImageView *barEdgeView = [[UIImageView alloc] initWithImage:imageEdge];
            [barEdgeView setFrame:CGRectMake(leftMargin + 49, 33, 172, 8)];
            
            if( percent > 0 )
            {
                UIView  *progressView = [[UIView alloc] initWithFrame:CGRectMake(3, 2, 166*percent/100, 4)];
                progressView.backgroundColor = [UIColor greenColor];
                [barEdgeView addSubview:progressView];
            }
            
            /*progress Bar */
            UIProgressView *personProgressBar = [[UIProgressView alloc] initWithFrame:CGRectMake(3, 3, 167, 8)];
            [personProgressBar setProgressViewStyle:UIProgressViewStyleBar];
            [personProgressBar setTrackTintColor: [UIColor clearColor]];
            if(item_status == 0) {
                [personProgressBar setProgressTintColor: [UIColor colorWithRed:0 green:255 blue:0 alpha:1]];
            } else {
                [personProgressBar setProgressTintColor: [UIColor grayColor]];
            }
            personProgressBar.progress = (float)(percent/100.0f);
            personProgressBar.tag = 6500;
            [personProgressBar setNeedsDisplay];
            [personProgressBar setHidden:YES];
	
            /*person Label */
            UILabel *percentLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin+ 230, 23, 40, 26)];
            percentLabel.text = [NSString stringWithFormat:@"%d%%",percent];
            percentLabel.font = titleFont;
            percentLabel.numberOfLines = 1;
            percentLabel.textColor = [UIColor whiteColor];
            percentLabel.textAlignment = NSTextAlignmentLeft;
            percentLabel.backgroundColor = [UIColor clearColor];
            
            /* person Image */

            NSString *imageUrl = [dict_content objectForKey:@"imageURL"];
            NSString *serverUrl = [NSString stringWithFormat:@"%@/pic/%@",IMAGE_SERVER_URL,imageUrl];
            NSURL *imageURL = [NSURL URLWithString:serverUrl];
                
            AsyncImageView* personImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(leftMargin  , 5, 43, 43)];
            [personImageView loadImageFromURL:imageURL];
            [button addSubview:personImageView];
            personImageView.image = [UIImage imageNamed:@"icon_user.png"];
            
            [button addSubview:titleLabel];
            [button addSubview:percentLabel];
            [button addSubview:barEdgeView];
            [barEdgeView addSubview:personProgressBar];
            
            button.tag = i + 1000;
            [button addTarget:self action: @selector(buttonClicked:) forControlEvents: UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
        }
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 53 * (m_voteItemCount+2))];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Faild" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (void)buttonClicked:(UIButton*)sender{
    
    KPGAppDelegate *delegate = (KPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    sender.selected = YES;
    int sender_id = (int)sender.tag - 1000;
    delegate.currentVoteItemIndex =sender_id;
    
    NSString *str_index = [NSString stringWithFormat:@"%d", sender_id];
    NSDictionary *dict_content = [delegate.voteItemList objectForKey:str_index];

    NSLog(@"Selected Vote Item -> #### %@",dict_content);
    
    NSString *vote_item_id = [dict_content objectForKey:@"voteItemid"];
    delegate.voteItemId = [vote_item_id integerValue];
    delegate.voteItemDescription = [dict_content objectForKey:@"description"];
    delegate.voteItemImage = [dict_content objectForKey:@"imageURL"];
    delegate.voteItemName = [dict_content objectForKey:@"name"];
    
    for (int i = 1; i <= m_voteItemCount; i++) {
        if (sender_id == i) {
            continue;
        } else {
            UIButton *button = (UIButton *)[self.scrollView viewWithTag:i+1000];
            button.selected = NO;
        }
    }

    UIProgressView *personProgressView = (UIProgressView *)[sender viewWithTag:6500];
    delegate.percentVote = (int)(personProgressView.progress * 100);
    
    VoteInfoViewController* voteInfoView = [[VoteInfoViewController alloc] initWithNibName:@"VoteInfoViewController" bundle:nil];
    [self.navigationController pushViewController:voteInfoView animated:YES];
}

@end
