//
//  SelectedVoteViewController.h
//  KPG
//
//  Created by MyAdmin on 3/26/14.
//  Copyright (c) 2014 MyAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedVoteViewController : UIViewController {
    int m_voteItemCount;
    int m_voteEventId;
    NSString* m_backgroundUrl;
    
    int leftMargin;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@end
